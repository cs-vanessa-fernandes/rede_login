/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LoginServiceTest.java
 * Descrição: LoginServiceTest.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Maite Balhester <mbalhest@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Refatoramento do nome da classe
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

import br.com.rede.ke.login.config.LoginConfig;
import br.com.rede.ke.login.exception.ConciliationLoginException;
import br.com.rede.ke.login.exception.FileRepositoryException;
import br.com.rede.ke.login.exception.TokenValidationException;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.model.request.PnLoginRequest;
import br.com.rede.ke.login.model.request.UserRequest;
import br.com.rede.ke.login.model.response.PnLoginResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import static br.com.rede.ke.login.exception.ConciliationLoginException.INTERNAL_SERVER_ERROR_MESSAGE;
import static br.com.rede.ke.login.exception.ConciliationLoginException.UNAUTHORIZED_MESSAGE;
import static br.com.rede.ke.login.exception.ConciliationLoginException.USER_BLOCKED_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

/**
 * The Class LoginServiceTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginServiceTest {

    @Autowired
    private LoginService loginService;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private LoginConfig loginConfig;

    /**
     * Before class.
     *
     * @throws TokenValidationException the token validation exception
     * @throws FileRepositoryException the file repository exception
     */
    @Before
    public void beforeClass() throws TokenValidationException, FileRepositoryException {
        loginService.init();
    }

    /**
     * Test login.
     *
     * @throws Exception the exception
     */
    @Test
    @Transactional
    public void testLogin() throws Exception {
        given(restTemplate.postForObject(anyString(), any(PnLoginRequest.class), any(Class.class)))
            .willReturn(loadPnResponse("/pn/success.json"));
        UserDTO user = loginService.login(UserRequest.from("a", "a", ""));

        assertNotNull("User must not be null", user);
        assertEquals("Invalid PV Count", 5, user.getPvList().size());
    }

    /**
     * Test login point.
     *
     * @throws Exception the exception
     */
    @Test
    @Transactional
    public void testLoginPoint() throws Exception {
        given(restTemplate.postForObject(anyString(), any(PnLoginRequest.class), any(Class.class)))
            .willReturn(loadPnResponse("/pn/success.json"));
        UserDTO user = loginService.loginPoint(UserRequest.from("a", "a", "", "12345678"));

        assertNotNull("User must not be null", user);
        assertEquals("Invalid PV Count", 1, user.getPvList().size());
    }

    /**
     * Test login single sign on.
     *
     * @throws Exception the exception
     */
    @Test
    @Transactional
    public void testLoginSingleSignOn() throws Exception {
        given(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(PnLoginResponse.class)))
            .willReturn(validUser());
        UserDTO user = loginService.singleSignon(UserRequest.from("", "", "12345678"));

        assertNotNull("User must not be null", user);
        assertEquals("Invalid PV Count", 5, user.getPvList().size());
    }

    /**
     * Test login single sign on error.
     *
     * @throws Exception the exception
     */
    @Test
    @Transactional
    public void testLoginSingleSignOnError() throws Exception {
        given(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), any(Class.class)))
            .willThrow(buildException(403, "/pn/user_blocked.json"));

        boolean gotException = false;

        try {
            loginService.singleSignon(UserRequest.from("", "", "12345678"));
        } catch (ConciliationLoginException e) {
            gotException = true;
            assertEquals(401, e.getStatusCode());
        }

        assertTrue("Expected exception but got none", gotException);
    }

    /**
     * Test login mock.
     *
     * @throws Exception the exception
     */
    @Test
    @Transactional
    public void testLoginMock() throws Exception {
        loginConfig.setMockEnabled(true);
        UserDTO user = loginService.loginMock(UserRequest.from("a", "a", ""));

        assertNotNull("User must not be null", user);
        assertEquals("Invalid PV Count", 5, user.getPvList().size());
        loginConfig.setMockEnabled(false);
    }

    /**
     * Test login mock disabled.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginMockDisabled() throws Exception {
        boolean gotException = false;
        try {
            loginService.loginMock(UserRequest.from("", "", ""));
        } catch (ConciliationLoginException e) {
            gotException = true;
            assertEquals(404, e.getStatusCode());
        }
        assertTrue("Should throw exception, but got none", gotException);
    }

    /**
     * Test login blocked.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginBlocked() throws Exception {
        testLoginError(403, "/pn/user_blocked.json", USER_BLOCKED_MESSAGE, 401);
    }

    /**
     * Test login unauthorized.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginUnauthorized() throws Exception {
        testLoginError(403, "/pn/unauthorized.json", UNAUTHORIZED_MESSAGE, 401);
    }

    /**
     * Test login unexpected error.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginUnexpectedError() throws Exception {
        testLoginError(500, "/pn/unauthorized.json", INTERNAL_SERVER_ERROR_MESSAGE);
    }

    /**
     * Test login unauthorized no pv.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginUnauthorizedNoPv() throws Exception {
        testLoginErrorPnOk("/pn/success_alt.json", 401, UNAUTHORIZED_MESSAGE);
    }

    /**
     * Test login token sig invalid.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginTokenSigInvalid() throws Exception {
        testLoginErrorPnOk("/pn/success_token_sig_invalid.json", 401, UNAUTHORIZED_MESSAGE);
    }

    /**
     * Test login token invalid.
     *
     * @throws Exception the exception
     */
    @Test
    public void testLoginTokenInvalid() throws Exception {
        testLoginErrorPnOk("/pn/success_token_invalid.json", 500, INTERNAL_SERVER_ERROR_MESSAGE);
    }

    /**
     * Test login error pn ok.
     *
     * @param jsonPath the json path
     * @param statusCode the status code
     * @param expectedMessage the expected message
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testLoginErrorPnOk(String jsonPath, int statusCode, String expectedMessage) throws IOException {
        given(restTemplate.postForObject(anyString(), any(PnLoginRequest.class), any(Class.class)))
            .willReturn(loadPnResponse(jsonPath));

        boolean gotException = false;
        try {
            loginService.login(UserRequest.from("a", "a", ""));
        } catch (ConciliationLoginException e) {
            assertEquals(statusCode, e.getStatusCode());
            assertEquals(expectedMessage, e.getMessage());
            gotException = true;
        }

        Assert.assertTrue("Expected exception but got none", gotException);
    }

    /**
     * Test login error.
     *
     * @param statusCode the status code
     * @param jsonPath the json path
     * @param expectedMessage the expected message
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testLoginError(Integer statusCode, String jsonPath, String expectedMessage) throws IOException {
        testLoginError(statusCode, jsonPath, expectedMessage, null);
    }

    /**
     * Test login error.
     *
     * @param statusCode the status code
     * @param jsonPath the json path
     * @param expectedMessage the expected message
     * @param expectedCode the expected code
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testLoginError(Integer statusCode, String jsonPath, String expectedMessage, Integer expectedCode)
        throws IOException {

        given(restTemplate.postForObject(anyString(), any(PnLoginRequest.class), any(Class.class)))
            .willThrow(buildException(statusCode, jsonPath));

        if (expectedCode == null) {
            expectedCode = statusCode;
        }

        boolean gotException = false;
        try {
            loginService.login(UserRequest.from("a", "a", ""));
        } catch (ConciliationLoginException e) {
            assertEquals(expectedCode, Integer.valueOf(e.getStatusCode()));
            assertEquals(expectedMessage, e.getMessage());
            gotException = true;
        }

        Assert.assertTrue("Expected exception but got none", gotException);
    }

    /**
     * Load pn response.
     *
     * @param jsonPath the json path
     * @return the pn login response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private PnLoginResponse loadPnResponse(String jsonPath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream inputStream = LoginServiceTest.class.getResourceAsStream(jsonPath)) {
            return objectMapper.readValue(inputStream, PnLoginResponse.class);
        }
    }

    /**
     * Builds the exception.
     *
     * @param statusCode the status code
     * @param jsonPath the json path
     * @return the rest client response exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private RestClientResponseException buildException(Integer statusCode, String jsonPath) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(LoginServiceTest.class.
            getResourceAsStream(jsonPath)))) {

            String responseBody = buffer.lines().collect(Collectors.joining("\n"));
            return new RestClientResponseException("Exception", statusCode, statusCode.toString(), null, responseBody
                .getBytes(), Charset.defaultCharset());
        }
    }

    /**
     * Valid user.
     *
     * @return the response entity
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private ResponseEntity<PnLoginResponse> validUser() throws IOException {
        PnLoginResponse body = loadPnResponse("/pn/success.json");

        ResponseEntity<PnLoginResponse> response = new ResponseEntity<PnLoginResponse>(body, HttpStatus.OK);

        return response;
    }
}
