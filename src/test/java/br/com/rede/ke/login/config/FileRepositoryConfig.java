/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileRepositoryConfig.java
 * Descrição: FileRepositoryConfig.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.config;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.rede.ke.login.util.FileRepository;
import br.com.rede.ke.login.util.FileRepositoryS3;
import br.com.rede.ke.login.util.TokenReader;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

/**
 * The Class FileRepositoryConfig.
 */
@Configuration
public class FileRepositoryConfig {

    /**
     * File repository S 3.
     *
     * @return the file repository
     * @throws Exception the exception
     */
    @Bean
    public FileRepository fileRepositoryS3() throws Exception {

        FileRepositoryS3 mock = Mockito.mock(FileRepositoryS3.class);
        given(mock.getBufferReader(anyString(), anyString())).will(new Answer<BufferedReader>() {
            @Override
            public BufferedReader answer(InvocationOnMock invocation) throws Throwable {
                InputStream resourceAsStream;
                if (invocation.getArguments()[1].equals(TokenReader.PUBLIC_KEY_LOCATION)) {
                    resourceAsStream = getClass().getResourceAsStream("/pn_public_key.rsa");
                } else {
                    resourceAsStream = getClass().getResourceAsStream("/pn/success.json");
                }

                return new BufferedReader(new InputStreamReader(resourceAsStream));
            }
        });
        return mock;
    }
}
