/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : ControllerTestApplication.java
 * Descrição: ControllerTestApplication.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.login.controller;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

//@SpringBootApplication(scanBasePackages = "br.com.conciliation.login")
/**
 * The Class ControllerTestApplication.
 */
//@EnableConfigurationProperties({ LoginConfig.class })
public class ControllerTestApplication extends SpringBootServletInitializer {

    private static Class<ControllerTestApplication> applicationClass = ControllerTestApplication.class;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

}
