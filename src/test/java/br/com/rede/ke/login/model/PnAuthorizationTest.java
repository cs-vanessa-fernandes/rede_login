/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PnAuthorizationTest.java
 * Descrição: PnAuthorizationTest.java.
 * Autor    : Mariana Bravo <mbravo@thoughtworks.com>
 * Data     : 23/12/2016
 * Empresa  : ThoughtWorks
 */
package br.com.rede.ke.login.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Map;

import org.assertj.core.util.Maps;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import br.com.rede.ke.login.model.response.Entidade;
import br.com.rede.ke.login.model.response.PnLoginResponse;
import br.com.rede.ke.login.util.TokenReader;

/**
 * The Class PnAuthorizationTest.
 */
public class PnAuthorizationTest {

    /**
     * Provides access token.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void providesAccessToken() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        pnLoginResponse.setAccessToken("accessToken");

        TokenReader tokenReader = mock(TokenReader.class);
        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.getAccessToken(), equalTo("accessToken"));
    }

    /**
     * Provides pv codes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void providesPvCodes() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        Entidade entidade1 = new Entidade();
        entidade1.setCodigo("123");
        Entidade entidade2 = new Entidade();
        entidade2.setCodigo("123456789");
        Entidade entidade3 = new Entidade();
        entidade3.setCodigo("1234567890");
        pnLoginResponse.setEntidades(Arrays.asList(entidade1, entidade2, entidade3));

        TokenReader tokenReader = mock(TokenReader.class);
        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.getRedePvCodes(), equalTo(Arrays.asList("000000123", "123456789", "1234567890")));
    }

    /**
     * Provides user email.
     *
     * @throws Exception the exception
     */
    @Test
    public void providesUserEmail() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        TokenReader tokenReader = mock(TokenReader.class);
        when(tokenReader.getClaim("usuario", Map.class)).thenReturn(Maps.newHashMap("Email", "bob@example.com"));

        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.getUserEmail(), equalTo("bob@example.com"));
    }

    /**
     * Checks for restricted access when atendimento flag is true.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void hasRestrictedAccessWhenAtendimentoFlagIsTrue() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        TokenReader tokenReader = mock(TokenReader.class);
        when(tokenReader.getClaim("usuario", Map.class)).thenReturn(Maps.newHashMap("Atendimento", true));

        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.hasRestrictedAccess(), equalTo(true));
    }

    /**
     * Does not have restricted access when atendimento flag is false.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void doesNotHaveRestrictedAccessWhenAtendimentoFlagIsFalse() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        TokenReader tokenReader = mock(TokenReader.class);
        when(tokenReader.getClaim("usuario", Map.class)).thenReturn(Maps.newHashMap("Atendimento", false));

        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.hasRestrictedAccess(), equalTo(false));
    }

    /**
     * Does not have restricted access when atendimento flag is null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void doesNotHaveRestrictedAccessWhenAtendimentoFlagIsNull() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        TokenReader tokenReader = mock(TokenReader.class);
        when(tokenReader.getClaim("usuario", Map.class)).thenReturn(Maps.newHashMap("Atendimento", null));

        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.hasRestrictedAccess(), equalTo(false));
    }

    /**
     * Does not have restricted access when atendimento flag is absent.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void doesNotHaveRestrictedAccessWhenAtendimentoFlagIsAbsent() throws Exception {
        PnLoginResponse pnLoginResponse = new PnLoginResponse();
        TokenReader tokenReader = mock(TokenReader.class);
        when(tokenReader.getClaim("usuario", Map.class)).thenReturn(ImmutableMap.of());

        PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);

        assertThat(authorization.hasRestrictedAccess(), equalTo(false));
    }
}
