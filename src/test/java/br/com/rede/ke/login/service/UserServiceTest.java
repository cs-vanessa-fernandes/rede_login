/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserServiceTest.java
 * Descrição: UserServiceTest.java.
 * Autor    : Geisly Conca <gconca@thoughtworks.com>
 * Data     : 26/12/2016
 * Empresa  : Thoughtworks
 */
package br.com.rede.ke.login.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import br.com.rede.ke.login.entity.Acquirer;
import br.com.rede.ke.login.entity.Pv;
import br.com.rede.ke.login.entity.User;
import br.com.rede.ke.login.model.PnAuthorization;
import br.com.rede.ke.login.model.PvDTO;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.repository.AcquirerRepository;
import br.com.rede.ke.login.repository.PvRepository;
import br.com.rede.ke.login.repository.UserRepository;

/**
 * Serviço para operações sobre pvs autorizados para o usuário no login.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    /** The Constant ACQUIRER_REDE. */
    private static final Acquirer ACQUIRER_REDE = Acquirer.with(1);

    /** The Constant ACQUIRER_CIELO. */
    private static final Acquirer ACQUIRER_CIELO = Acquirer.with(2);

    /** The acquirer repository. */
    @Mock
    private AcquirerRepository acquirerRepository;

    /** The pv repository. */
    @Mock
    private PvRepository pvRepository;

    /** The user repository. */
    @Mock
    private UserRepository userRepository;

    /** The service. */
    @InjectMocks
    private UserService service;

    /**
     * Cria UserDTO a partir de um token de acesso.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWithAccessToken() throws Exception {
        String accessToken = "12345";

        PnAuthorization pnAuthorization = mock(PnAuthorization.class);
        when(pnAuthorization.getAccessToken()).thenReturn(accessToken);

        mockUserRepositoryFindByEmail("bob@example.com");

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        assertThat(userDTO.getToken(), equalTo(accessToken));
    }

    /**
     * Cria um UserDTO com pvs rede autorizados.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWithRedePvCodes() throws Exception {
        ImmutableList<String> codes = ImmutableList.of("67822");

        PnAuthorization pnAuthorization = mock(PnAuthorization.class);
        when(pnAuthorization.getRedePvCodes()).thenReturn(codes);

        mockRepositoryFindRedePvs(codes, createBranchPv(11L, "67822", ACQUIRER_REDE));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        PvDTO expectedPvDTO = createPvDTO(11L, "67822", ACQUIRER_REDE.getId());

        assertThat(userDTO.getPvList(), equalTo(ImmutableList.of(expectedPvDTO)));
    }

    /**
     * Cria um UserDTO com pvs autorizados Cielo.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWithCieloPvCodes() throws Exception {
        PnAuthorization pnAuthorization = mockPnAuthorizationCalls(null, "bob@example.com", Boolean.FALSE);

        mockUserRepositoryFindByEmail("bob@example.com", createBranchPv(12L, "34563", ACQUIRER_CIELO));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        PvDTO expectedPvDTO = createPvDTO(12L, "34563", ACQUIRER_CIELO.getId());
        assertThat(userDTO.getPvList(), equalTo(ImmutableList.of(expectedPvDTO)));
    }

    /**
     * Cria um UserDTO com pvs matriz e filiais.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWithBranchesAndHeadquarters() throws Exception {
        ImmutableList<String> codes = ImmutableList.of("89980", "12345");
        String userEmail = "dog@example.com";

        PnAuthorization pnAuthorization = mockPnAuthorizationCalls(codes, userEmail, Boolean.FALSE);

        mockRepositoryFindRedePvs(codes,
            createHeadquarterPv(11L, "89980", ACQUIRER_REDE, createBranchPv(20L, "98392", ACQUIRER_REDE)),
            createBranchPv(13L, "12345", ACQUIRER_REDE));

        mockUserRepositoryFindByEmail(userEmail,
            createHeadquarterPv(9L, "13256", ACQUIRER_CIELO,
            createBranchPv(8L, "63827", ACQUIRER_CIELO)));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        ImmutableList<PvDTO> expectedPvDTO = ImmutableList.of(
            createPvDTO(11L, "89980", ACQUIRER_REDE.getId()),
            createPvDTO(20L, "98392", ACQUIRER_REDE.getId()),
            createPvDTO(13L, "12345", ACQUIRER_REDE.getId()),
            createPvDTO(9L, "13256", ACQUIRER_CIELO.getId()),
            createPvDTO(8L, "63827", ACQUIRER_CIELO.getId()));

        assertThat(userDTO.getPvList(), equalTo(expectedPvDTO));
    }

    /**
     * Cria um UserDTO com filtro de pvs.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWhenFilterIsPresentThenOnlyShowPvsRede() throws Exception {
        ImmutableList<String> codes = ImmutableList.of("67895", "99837");
        String userEmail = "bob@example.com";

        PnAuthorization pnAuthorization = mockPnAuthorizationCalls(codes, userEmail, Boolean.FALSE);

        mockUserRepositoryFindByEmail(userEmail, createBranchPv(3L, "67895", ACQUIRER_CIELO));

        mockRepositoryFindRedePvs(codes, createBranchPv(2L, "67895", ACQUIRER_REDE),
            createBranchPv(6L, "99837", ACQUIRER_REDE));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, ImmutableList.of("67895"));

        ImmutableList<PvDTO> expectedPvDTO = ImmutableList.of(
            createPvDTO(2L, "67895", ACQUIRER_REDE.getId()));

        assertThat(userDTO.getPvList(), equalTo(expectedPvDTO));
    }

    /**
     * Cria um UserDTO para um usuário de atendimento.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWhenIsAtendimentoThenOnlyShowPvsRede() throws Exception {
        ImmutableList<String> codes = ImmutableList.of("64829");
        String userEmail = "bob@example.com";

        PnAuthorization pnAuthorization = mockPnAuthorizationCalls(codes, userEmail, Boolean.TRUE);

        mockUserRepositoryFindByEmail(userEmail, createBranchPv(21L, "38726", ACQUIRER_CIELO));

        mockRepositoryFindRedePvs(codes, createBranchPv(87L, "64829", ACQUIRER_REDE));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        ImmutableList expectedPvDTO = ImmutableList.of(createPvDTO(87L, "64829", ACQUIRER_REDE.getId()),
            createPvDTO(21L, "38726", ACQUIRER_CIELO.getId()));

        assertThat(userDTO.getPvList(), equalTo(expectedPvDTO));
    }

    /**
     * Mock pn authorization calls.
     *
     * @param codes the codes
     * @param userEmail the user email
     * @param restrictedAccess the restricted access
     * @return the pn authorization
     */
    private PnAuthorization mockPnAuthorizationCalls(
        ImmutableList<String> codes,
        String userEmail,
        boolean restrictedAccess) {
        PnAuthorization pnAuthorization = mock(PnAuthorization.class);
        when(pnAuthorization.getUserEmail()).thenReturn(userEmail);
        when(pnAuthorization.getRedePvCodes()).thenReturn(codes);
        when(pnAuthorization.hasRestrictedAccess()).thenReturn(restrictedAccess);
        return pnAuthorization;
    }

    /**
     * Cria um UserDTO sem n-plicação de pvs.
     *
     * @throws Exception
     *             caso um erro inesperado aconteça
     */
    @Test
    public void createsUserDTOWhenPNReturnsCodeThatIsAlsoABranchOfOtherCodeReturned() throws Exception {
        ImmutableList<String> codes = ImmutableList.of("78836", "92819");

        PnAuthorization pnAuthorization = mock(PnAuthorization.class);
        when(pnAuthorization.getRedePvCodes()).thenReturn(codes);

        mockRepositoryFindRedePvs(codes,
            createHeadquarterPv(7L, "78836", ACQUIRER_REDE, createBranchPv(4L, "92819", ACQUIRER_REDE)),
            createBranchPv(4L, "92819", ACQUIRER_REDE));

        UserDTO userDTO = service.fromPnAuthorization(pnAuthorization, null);

        ImmutableList expectedPvDTO = ImmutableList.of(
            createPvDTO(7L, "78836", ACQUIRER_REDE.getId()),
            createPvDTO(4L, "92819", ACQUIRER_REDE.getId()));

        assertThat(userDTO.getPvList(), equalTo(expectedPvDTO));
    }

    /**
     * Save user.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveUser() throws Exception {
        User user = new User();
        user.setEmail("unittesting@userede.com.br");

        mockUserRepositorySaveUser(user);

        assertTrue(service.saveUser(user.getEmail()));
    }

    /**
     * Mock repository find rede pvs.
     *
     * @param codes the codes
     * @param pvs the pvs
     */
    private void mockRepositoryFindRedePvs(ImmutableList<String> codes, Pv... pvs) {
        when(acquirerRepository.findByName(anyString())).thenReturn(ACQUIRER_REDE);
        when(pvRepository.findByAcquirerAndCodeIn(ACQUIRER_REDE, codes)).thenReturn(
            ImmutableList.copyOf(pvs));
    }

    /**
     * Mock user repository find by email.
     *
     * @param userEmail the user email
     * @param pvs the pvs
     */
    private void mockUserRepositoryFindByEmail(String userEmail, Pv... pvs) {
        User user = new User();
        user.setEmail(userEmail);
        user.setPvs(ImmutableSet.copyOf(pvs));
        when(userRepository.findByEmail(userEmail)).thenReturn(user);
    }

    /**
     * Mock user repository save user.
     *
     * @param user the user
     */
    private void mockUserRepositorySaveUser(User user) {
        when(userRepository.save(user)).thenReturn(user);
    }

    /**
     * Creates the pv DTO.
     *
     * @param id the id
     * @param code the code
     * @param acquirerId the acquirer id
     * @return the pv DTO
     */
    private PvDTO createPvDTO(long id, String code, int acquirerId) {
        PvDTO pvDTO = new PvDTO();
        pvDTO.setCode(code);
        pvDTO.setId(id);
        pvDTO.setAcquirerId(acquirerId);
        return pvDTO;
    }

    /**
     * Creates the branch pv.
     *
     * @param id the id
     * @param code the code
     * @param acquirer the acquirer
     * @return the pv
     */
    private Pv createBranchPv(long id, String code, Acquirer acquirer) {
        Pv pv = createPv(id, code, acquirer);
        pv.setHeadquarter(new Pv());
        return pv;
    }

    /**
     * Creates the headquarter pv.
     *
     * @param id the id
     * @param code the code
     * @param acquirer the acquirer
     * @param pvBranches the pv branches
     * @return the pv
     */
    private Pv createHeadquarterPv(long id, String code, Acquirer acquirer, Pv... pvBranches) {
        Pv pv = createPv(id, code, acquirer);
        pv.setBranches(ImmutableSet.copyOf(pvBranches));
        return pv;
    }

    /**
     * Creates the pv.
     *
     * @param id the id
     * @param code the code
     * @param acquirer the acquirer
     * @return the pv
     */
    private Pv createPv(long id, String code, Acquirer acquirer) {
        Pv pv = new Pv();
        pv.setId(id);
        pv.setCode(code);
        pv.setAcquirer(acquirer);
        return pv;
    }
}