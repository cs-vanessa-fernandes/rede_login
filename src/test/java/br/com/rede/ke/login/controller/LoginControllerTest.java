/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LoginControllerTest.java
 * Descrição: LoginControllerTest.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Maite Balhester <mbalhest@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Refatoramento do nome da classe
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.controller;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import br.com.rede.ke.login.exception.ConciliationLoginException;
import br.com.rede.ke.login.model.PvDTO;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.model.request.UserRequest;
import br.com.rede.ke.login.service.LoginService;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The Class LoginControllerTest.
 */
@RunWith(SpringRunner.class)
@WebMvcTest({LoginController.class})
public class LoginControllerTest {

    /**
     * The Enum EndpointEnum.
     */
    private enum EndpointEnum {
        LOGIN("/login"),
        MOCK("/mock"),
        SINGLE_SIGN_ON("/singlesignon"),
        LOGIN_POINT("/login/pv");

        private String endpoint;

        /**
         * Instantiates a new endpoint enum.
         *
         * @param endpoint the endpoint
         */
        EndpointEnum(String endpoint) {
            this.endpoint = endpoint;
        }
    }

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LoginService loginService;

    /**
     * Login.
     *
     * @throws Exception the exception
     */
    @Test
    public void login() throws Exception {
        doTestLogin(EndpointEnum.LOGIN);
    }

    /**
     * Single signon.
     *
     * @throws Exception the exception
     */
    @Test
    public void singleSignon() throws Exception {
        doTestSingleSignon(EndpointEnum.SINGLE_SIGN_ON);
    }

    /**
     * Single signon error 403.
     *
     * @throws Exception the exception
     */
    @Test
    public void singleSignonError403() throws Exception {
        doTestSingleSignonError(EndpointEnum.SINGLE_SIGN_ON, 403);
    }

    /**
     * Login 403.
     *
     * @throws Exception the exception
     */
    @Test
    public void login403() throws Exception {
        doTestLoginError(EndpointEnum.LOGIN, "$.status", 403);
    }

    /**
     * Login mock.
     *
     * @throws Exception the exception
     */
    @Test
    public void loginMock() throws Exception {
        doTestLogin(EndpointEnum.MOCK);
    }

    /**
     * Login mock disabled.
     *
     * @throws Exception the exception
     */
    @Test
    public void loginMockDisabled() throws Exception {
        doTestLoginError(EndpointEnum.MOCK, null, 404);
    }

    /**
     * Login mock error.
     *
     * @throws Exception the exception
     */
    @Test
    public void loginMockError() throws Exception {
        doTestLoginError(EndpointEnum.MOCK, "$.status", 403);
    }

    /**
     * Login with pv.
     *
     * @throws Exception the exception
     */
    @Test
    public void loginWithPv() throws Exception {
        doTestLogin(EndpointEnum.LOGIN_POINT);
    }

    /**
     * Login with pv error.
     *
     * @throws Exception the exception
     */
    @Test
    public void loginWithPvError() throws Exception {
        doTestLoginError(EndpointEnum.LOGIN_POINT, "$.status", 401);
    }

    /**
     * Do test login.
     *
     * @param endpoint the endpoint
     * @throws Exception the exception
     */
    private void doTestLogin(EndpointEnum endpoint) throws Exception {
        switch (endpoint) {
            case LOGIN:
                given(loginService.login(any(UserRequest.class))).willReturn(new UserDTO("", Collections
                    .singletonList(new PvDTO())));
                break;
            case MOCK:
                given(loginService.loginMock(any(UserRequest.class))).willReturn(new UserDTO("", Collections
                    .singletonList(new PvDTO())));
                break;
            case LOGIN_POINT:
                given(loginService.loginPoint(any(UserRequest.class))).willReturn(new UserDTO("", Collections
                    .singletonList(new PvDTO())));
                break;
            default:
        }

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(UserRequest.from("", "", ""));

        mvc.perform(post(endpoint.endpoint)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(json))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.user.pvList", hasSize(1)));
    }

    /**
     * Do test single signon.
     *
     * @param endpoint the endpoint
     * @throws Exception the exception
     */
    private void doTestSingleSignon(EndpointEnum endpoint) throws Exception {
        String expectedAuthorization = "TESTAUTHORIZATION";

        given(loginService.singleSignon(any(UserRequest.class))).willReturn(new UserDTO(expectedAuthorization,
            Collections.singletonList(new PvDTO())));

        mvc.perform(post(endpoint.endpoint).header("authorization", expectedAuthorization)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.user.token").isNotEmpty())
            .andExpect(jsonPath("$.user.pvList", hasSize(1)));
    }

    /**
     * Do test login error.
     *
     * @param endpoint the endpoint
     * @param jsonPath the json path
     * @param errorCode the error code
     * @throws Exception the exception
     */
    private void doTestLoginError(EndpointEnum endpoint, String jsonPath, int errorCode) throws Exception {
        switch (endpoint) {
            case LOGIN:
                given(loginService.login(any(UserRequest.class))).willThrow(new ConciliationLoginException("",
                    errorCode));
                break;
            case MOCK:
                given(loginService.loginMock(any(UserRequest.class))).willThrow(new ConciliationLoginException("",
                    errorCode));
                break;
            case LOGIN_POINT:
                given(loginService.loginPoint(any(UserRequest.class))).willThrow(new ConciliationLoginException("",
                    errorCode));
                break;
            default:
        }

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(UserRequest.from("", "", ""));

        ResultActions resultActions = mvc.perform(post(endpoint.endpoint)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(json))
            .andExpect(status().is4xxClientError());

        if (jsonPath != null) {
            resultActions.andExpect(jsonPath(jsonPath, is(errorCode)));
        }
    }

    /**
     * Do test single signon error.
     *
     * @param endpoint the endpoint
     * @param errorCode the error code
     * @throws Exception the exception
     */
    private void doTestSingleSignonError(EndpointEnum endpoint, int errorCode) throws Exception {
        given(loginService.singleSignon(any(UserRequest.class))).willThrow(new ConciliationLoginException("",
            errorCode));

        mvc.perform(post(endpoint.endpoint)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().is4xxClientError());
    }
}