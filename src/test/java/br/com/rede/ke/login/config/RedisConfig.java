/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : RedisConfig.java
 * Descrição: RedisConfig.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.login.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializer;

import br.com.rede.ke.login.nosql.TokenRedisRepository;
import br.com.rede.ke.login.nosql.TokenRedisTemplate;

// TODO: Auto-generated Javadoc
/**
 * The Class RedisConfig.
 */
@Configuration
public class RedisConfig {

    /**
     * Default redis serializer.
     *
     * @return the redis serializer
     */
    @Bean
    @SuppressWarnings("unchecked")
    public RedisSerializer<Object> defaultRedisSerializer() {
        return Mockito.mock(RedisSerializer.class);
    }

    /**
     * Connection factory.
     *
     * @return the redis connection factory
     */
    @Bean
    public RedisConnectionFactory connectionFactory() {
        RedisConnectionFactory factory = Mockito.mock(RedisConnectionFactory.class);
        RedisConnection connection = Mockito.mock(RedisConnection.class);
        Mockito.when(factory.getConnection()).thenReturn(connection);

        return factory;
    }

    /**
     * Token redis template.
     *
     * @return the token redis template
     */
    @Bean
    public TokenRedisTemplate tokenRedisTemplate() {
        return new TokenRedisTemplate(connectionFactory());
    }

    /**
     * Token redis repository.
     *
     * @return the token redis repository
     */
    @Bean
    public TokenRedisRepository tokenRedisRepository() {
        return new TokenRedisRepository();
    }
}