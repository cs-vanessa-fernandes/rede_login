/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : TokenValidationException.java
 * Descrição: TokenValidationException.
 * Autor    : Gustavo Silva <gustavo.silva@iteris.com.br>
 * Data     : 30/11/2016
 * Empresa  : Iteris
 */
package br.com.rede.ke.login.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenValidationException.
 */
public class TokenValidationException extends Exception {

    /**
     * Instantiates a new token validation exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public TokenValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
