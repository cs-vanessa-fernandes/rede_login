/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileRepositoryS3.java
 * Descrição: FileRepositoryS3.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 06/03/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 191300
 *************************************************************
 */
package br.com.rede.ke.login.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.rede.ke.login.config.LoginConfig;
import br.com.rede.ke.login.exception.FileRepositoryException;
import com.amazonaws.AmazonClientException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;

/**
 * The Class FileRepositoryS3.
 */
@Component
public class FileRepositoryS3 implements FileRepository {

    private AmazonS3 s3;

    /**
     * Instantiates a new file repository S 3.
     * @param loginConfig login configuration
     */
    @Autowired
    public FileRepositoryS3(LoginConfig loginConfig) {
        if (loginConfig.isProxyEnabled()) {
            ClientConfiguration clientConfiguration = new ClientConfiguration();
            clientConfiguration.setProxyHost(loginConfig.getProxyHost());
            clientConfiguration.setProxyPort(loginConfig.getProxyPort());
            s3 = new AmazonS3Client(clientConfiguration);
        } else {
            s3 = new AmazonS3Client();
        }
        Region region = Region.getRegion(Regions.US_EAST_1);
        s3.setRegion(region);
    }

    /**
     * Gets the object.
     *
     * @param bucketName
     *            the bucket name
     * @param key
     *            the key
     * @return the object
     * @throws FileRepositoryException
     *             the file repository exception
     */
    private S3Object getObject(String bucketName, String key) throws FileRepositoryException {
        S3Object object = null;

        try {
            object = s3.getObject(new GetObjectRequest(bucketName, key));
            return object;

        } catch (AmazonS3Exception e) {
            throw new FileRepositoryException(String.format("Error getting object: %s/%s", bucketName, key));
        }
    }

    @Override
    public void cutFile(String bucketNameSource, String pathSource, String bucketNameTarget, String pathTarget)
        throws FileRepositoryException {

        copyFileS3(bucketNameSource, pathSource, bucketNameTarget, pathTarget);
        deleteFile(bucketNameSource, pathSource);
    }

    @Override
    public void copyFile(String bucketNameSource, String pathSource, String bucketNameTarget, String pathTarget)
        throws FileRepositoryException {
        copyFileS3(bucketNameSource, pathSource, bucketNameTarget, pathTarget);
    }

    @Override
    public int upLoadFile(String bucketName, InputStream input, String path) throws FileRepositoryException {
        int countErro = 0;
        boolean sucess = false;

        input.mark(2147483640);

        byte[] contentBytes;
        int contentLength = 0;

        do {
            try {
                contentBytes = IOUtils.toByteArray(input);
                contentLength = contentBytes.length;

                input.reset();

                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(contentLength);
                s3.putObject(bucketName, path, input, metadata);
                sucess = true;

            } catch (AmazonS3Exception e) {
                e.printStackTrace();
                countErro++;
            } catch (AmazonClientException e) {
                e.printStackTrace();
                countErro++;
            } catch (IOException e1) {
                e1.printStackTrace();
                countErro++;
            }
        } while (countErro < 3 && !sucess);

        if (!sucess) {
            throw new FileRepositoryException(String.format("Error uploading object: %s/%s", bucketName, path));
        }

        return contentLength;
    }

    @Override
    public void copyFileS3(String bucketNameSource, String sourceKey, String bucketNameDestiny, String destinationKey)
        throws FileRepositoryException {

        int countErro = 0;
        boolean sucess = false;
        do {
            try {
                s3.copyObject(bucketNameSource, sourceKey, bucketNameDestiny, destinationKey);
                sucess = true;

            } catch (AmazonClientException e) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e1) {
                    throw new FileRepositoryException(e);
                }
                countErro++;
            }

        } while (countErro < 3 && !sucess);

        if (!sucess) {
            throw new FileRepositoryException(String.format("Error copying object %s/%s to %s/%s", bucketNameSource,
                sourceKey, bucketNameDestiny, destinationKey));
        }
    }

    /**
     * Delete file.
     *
     * @param bucketName
     *            the bucket name
     * @param path
     *            the path
     * @throws FileRepositoryException
     *             the file repository exception
     */
    private void deleteFile(String bucketName, String path) throws FileRepositoryException {

        int countErro = 0;
        boolean sucess = false;

        do {
            try {
                s3.deleteObject(bucketName, path);
                sucess = true;

            } catch (AmazonClientException e) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e1) {
                    throw new FileRepositoryException(e);
                }
                countErro++;
            }

        } while (countErro < 3 && !sucess);

        if (!sucess) {
            throw new FileRepositoryException(String.format("Error removing object: %s/%s", bucketName, path));
        }
    }

    @Override
    public BufferedReader getBufferReader(String bucket, String path, String fileName) throws FileRepositoryException {

        BufferedReader buffer = new BufferedReader(new InputStreamReader(getContent(bucket, path + "/" + fileName)));
        return buffer;
    }

    /**
     * Gets the content.
     *
     * @param bucket
     *            the bucket
     * @param key
     *            the key
     * @return the content
     * @throws FileRepositoryException
     *             the file repository exception
     */
    private InputStream getContent(String bucket, String key) throws FileRepositoryException {
        S3Object content = null;

        try {
            content = getObject(bucket, key);
        } catch (FileRepositoryException e) {
            throw new FileRepositoryException(String.format("Error getting object content: %s/%s", bucket, key));
        }
        return content.getObjectContent();
    }

    @Override
    public BufferedReader getBufferReader(String bucket, String key) throws FileRepositoryException {

        BufferedReader buffer = new BufferedReader(new InputStreamReader(getContent(bucket, key)));
        return buffer;
    }

}
