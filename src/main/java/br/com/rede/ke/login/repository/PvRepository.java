/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PvRepository.java
 * Descrição: PvRepository.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Busca de PVs de outras adquirentes
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.repository;

import java.util.List;

import br.com.rede.ke.login.entity.Acquirer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rede.ke.login.entity.Pv;

/**
 * The Interface PvRepository.
 */
@Repository
public interface PvRepository extends CrudRepository<Pv, Long> {

    /**
     * Find by code in.
     *
     * @param codes the codes
     * @return the list
     */
    List<Pv> findByCodeIn(List<String> codes);

    /**
     * Find by headquarter id in.
     *
     * @param headquarterIds the headquarter ids
     * @return the list
     */
    // CHECKSTYLE.OFF: MethodName - Required by Spring Data
    List<Pv> findByHeadquarter_IdIn(List<Long> headquarterIds);
    // CHECKSTYLE.ON: MethodName

    /**
     * Busca PVs com adquirente e algum dos códigos especificados.
     *
     * @param acquirer a adquirente
     * @param codes os códigos especificados
     * @return lista de Pvs
     */
    List<Pv> findByAcquirerAndCodeIn(Acquirer acquirer, List<String> codes);
}
