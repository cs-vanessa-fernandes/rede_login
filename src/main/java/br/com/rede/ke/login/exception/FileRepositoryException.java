/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileRepositoryException.java
 * Descrição: FileRepositoryException.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.exception;

/**
 * The Class FileRepositoryException.
 */
public class FileRepositoryException extends Exception {

    /**
     * Instantiates a new file repository exception.
     *
     * @param cause the cause
     */
    public FileRepositoryException(Exception cause) {
        super(cause);
    }

    /**
     * Instantiates a new file repository exception.
     *
     * @param message the message
     */
    public FileRepositoryException(String message) {
        super(message);
    }
}