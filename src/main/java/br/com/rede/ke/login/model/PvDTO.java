/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PvDTO.java
 * Descrição: PvDTO.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model;

import java.util.Objects;

import br.com.rede.ke.login.entity.Pv;

/**
 * The Class PvDTO.
 */
public class PvDTO {

    private String code;
    private Long id;
    private Integer acquirerId;

    /**
     * Instantiates a new pv DTO.
     */
    public PvDTO() {
    }

    /**
     * Instantiates a new pv DTO.
     *
     * @param pv the pv
     */
    public PvDTO(Pv pv) {
        this.code = pv.getCode();
        this.id = pv.getId();
        this.acquirerId = pv.getAcquirer().getId();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the acquirer id.
     *
     * @return the acquirer id
     */
    public Integer getAcquirerId() {
        return acquirerId;
    }

    /**
     * Sets the acquirer id.
     *
     * @param acquirerId the new acquirer id
     */
    public void setAcquirerId(Integer acquirerId) {
        this.acquirerId = acquirerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PvDTO pvDTO = (PvDTO) o;
        return Objects.equals(code, pvDTO.code)
            && Objects.equals(id, pvDTO.id)
            && Objects.equals(acquirerId, pvDTO.acquirerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, id, acquirerId);
    }
}
