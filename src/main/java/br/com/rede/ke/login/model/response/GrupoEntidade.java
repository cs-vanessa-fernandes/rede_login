/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : GrupoEntidade.java
 * Descrição: GrupoEntidade.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.response;

/**
 * The Class GrupoEntidade.
 */
public class GrupoEntidade {
    private String codigo;

    /**
     * Gets the codigo.
     *
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the codigo.
     *
     * @param codigo the new codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
