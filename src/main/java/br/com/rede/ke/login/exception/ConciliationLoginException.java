/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : ConciliationLoginException.java
 * Descrição: ConciliationLoginException.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.exception;

/**
 * The Class ConciliationLoginException.
 */
public class ConciliationLoginException extends Exception {
    public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Erro Interno do servidor. Por favor, tente mais tarde";
    public static final String UNAUTHORIZED_MESSAGE = "Acesso negado/Usuário não autorizado.";
    public static final String USER_BLOCKED_MESSAGE = "Usuário bloqueado.";
    public static final String WRONG_CREDENTIALS_MESSAGE = "Usuário e/ou senha não informados.";
    public static final String WRONG_AUTHORIZATION_MESSAGE = "Sessão inválida.";

    private final int statusCode;

    /**
     * Instantiates a new conciliation login exception.
     *
     * @param message the message
     * @param statusCode the status code
     */
    public ConciliationLoginException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    /**
     * Instantiates a new conciliation login exception.
     *
     * @param message the message
     * @param statusCode the status code
     * @param cause the cause
     */
    public ConciliationLoginException(String message, int statusCode, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * Gets the status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return statusCode;
    }
}
