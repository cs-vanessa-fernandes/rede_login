/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : Entidade.java
 * Descrição: Entidade.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class Entidade.
 */
public class Entidade {

    private String codigo;
    private String nome;

    @JsonProperty("grupo_entidade")
    private GrupoEntidade grupoEntidade;

    /**
     * Gets the codigo.
     *
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the codigo.
     *
     * @param codigo the new codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Gets the nome.
     *
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the nome.
     *
     * @param nome the new nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Gets the grupo entidade.
     *
     * @return the grupo entidade
     */
    public GrupoEntidade getGrupoEntidade() {
        return grupoEntidade;
    }

    /**
     * Sets the grupo entidade.
     *
     * @param grupoEntidade the new grupo entidade
     */
    public void setGrupoEntidade(GrupoEntidade grupoEntidade) {
        this.grupoEntidade = grupoEntidade;
    }
}
