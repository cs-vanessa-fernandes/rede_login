/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserResponse.java
 * Descrição: UserResponse.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Maite Balhester <mbalhest@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Refatoramento do nome da classe
 *ID       : AM 188840
 *************************************************************
 */

package br.com.rede.ke.login.model.response;

import br.com.rede.ke.login.model.UserDTO;
import com.fasterxml.jackson.annotation.JsonInclude;

// TODO: Auto-generated Javadoc
/**
 * The Class UserResponse.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {
    private UserDTO user;
    private Integer status;
    private String message;

    /**
     * Instantiates a new user response.
     *
     * @param user the user
     */
    public UserResponse(UserDTO user) {
        this.user = user;
    }

    /**
     * Instantiates a new user response.
     *
     * @param status the status
     * @param message the message
     */
    public UserResponse(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public UserDTO getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user            the user to set
     */
    public void setUser(UserDTO user) {
        this.user = user;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
