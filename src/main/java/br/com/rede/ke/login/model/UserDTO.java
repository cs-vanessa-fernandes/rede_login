/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserDTO.java
 * Descrição: UserDTO.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Maite Balhester <mbalhest@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Refatoramento do nome da classe
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class UserDTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {
    private List<PvDTO> pvList;
    private String token;
    private String login;

    /**
     * Instantiates a new user.
     */
    public UserDTO() {
    }

    /**
     * Instantiates a new user.
     *
     * @param login
     *            the login
     */
    public UserDTO(String login) {
        this.login = login;
    }

    /**
     * Instantiates a new user.
     *
     * @param accessToken the access token
     * @param pvList the pv list
     */
    public UserDTO(String accessToken, List<PvDTO> pvList) {
        this.pvList = pvList;
        this.token = accessToken;
    }

    /**
     * Instantiates a new user.
     *
     * @param login the login
     * @param accessToken the access token
     * @param pvList the pv list
     */
    public UserDTO(String login, String accessToken, List<PvDTO> pvList) {
        this.login = login;
        this.pvList = pvList;
        this.token = accessToken;
    }

    /**
     * Gets the pv list.
     *
     * @return the pvList
     */
    public List<PvDTO> getPvList() {
        return pvList;
    }

    /**
     * Sets the pv list.
     *
     * @param pvList the pvList to set
     */
    public void setPvList(List<PvDTO> pvList) {
        this.pvList = pvList;
    }

    /**
     * Gets the token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the token.
     *
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login.
     *
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }
}
