/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : Application.java
 * Descrição: Application.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 06/03/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 191300
 *************************************************************
 */
package br.com.rede.ke.login;

import java.net.InetSocketAddress;
import java.net.Proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import br.com.rede.ke.login.config.LoginConfig;

/**
 * The Class Application.
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties({LoginConfig.class})
public class Application extends SpringBootServletInitializer {

    private static Class<Application> applicationClass = Application.class;

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    /**
     * Rest template.
     *
     * @param loginConfig login configuration
     *
     * @return the rest template
     */
    @Bean
    public RestTemplate restTemplate(LoginConfig loginConfig) {
        RestTemplate restTemplate;
        if (loginConfig.isProxyEnabled()) {
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(loginConfig.getProxyHost(), loginConfig
                .getProxyPort()));
            requestFactory.setProxy(proxy);
            restTemplate = new RestTemplate(requestFactory);
        } else {
            restTemplate = new RestTemplate();
        }

        return restTemplate;
    }

}
