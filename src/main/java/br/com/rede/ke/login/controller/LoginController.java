/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LoginController.java
 * Descrição: LoginController.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.rede.ke.login.Types;
import br.com.rede.ke.login.exception.ConciliationLoginException;
import br.com.rede.ke.login.exception.ResourceNotFoundException;
import br.com.rede.ke.login.model.request.UserRequest;
import br.com.rede.ke.login.model.response.UserResponse;
import br.com.rede.ke.login.service.LoginService;

/**
 * The Class LoginController.
 */
@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;

    /**
     * Login.
     *
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = {"application/json; charset=UTF-8"})
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest userRequest, HttpServletRequest request) {
        try {
            userRequest.setRequest(request);
            userRequest.setLoginType(Types.LOGIN);

            return ResponseEntity.ok(new UserResponse(loginService.login(userRequest)));
        } catch (ConciliationLoginException e) {
            e.printStackTrace();
            return ResponseEntity.status(e.getStatusCode()).body(new UserResponse(e.getStatusCode(), e.getMessage()));
        }
    }

    /**
     * Login mock.
     *
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/mock")
    public ResponseEntity<UserResponse> loginMock(@RequestBody UserRequest userRequest, HttpServletRequest request) {
        try {
            userRequest.setRequest(request);
            userRequest.setLoginType(Types.MOCK);

            return ResponseEntity.ok(new UserResponse(loginService.loginMock(userRequest)));
        } catch (ConciliationLoginException e) {
            if (e.getStatusCode() != 404) {
                e.printStackTrace();
                return ResponseEntity.status(e.getStatusCode()).body(new UserResponse(e.getStatusCode(),
                    e.getMessage()));
            }
            throw new ResourceNotFoundException();
        }
    }

    /**
     * Single signon.
     *
     * @param authorization the authorization
     * @return the response entity
     */
    @RequestMapping(value = "/singlesignon", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> singleSignon(@RequestHeader String authorization, HttpServletRequest request) {
        try {
            if (authorization != null) {
                UserRequest userRequest = new UserRequest();
                userRequest.setAuthorization(authorization);
                userRequest.setRequest(request);
                userRequest.setLoginType(Types.SINGLE_SIGNON);

                return ResponseEntity.ok(new UserResponse(loginService.singleSignon(userRequest)));
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new UserResponse(HttpStatus.BAD_GATEWAY
                    .value(), "Invalid authorization data"));
            }
        } catch (ConciliationLoginException e) {
            e.printStackTrace();
            return ResponseEntity.status(e.getStatusCode()).body(new UserResponse(e.getStatusCode(), e.getMessage()));
        }
    }

    /**
     * Login point.
     *
     * @param userRequest the user request
     * @return the response entity
     */
    @RequestMapping(value = "/login/pv", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> loginPoint(@RequestBody UserRequest userRequest, HttpServletRequest request) {
        try {
            userRequest.setRequest(request);
            userRequest.setLoginType(Types.POYNT);

            return ResponseEntity.ok(new UserResponse(loginService.loginPoint(userRequest)));
        } catch (ConciliationLoginException e) {
            e.printStackTrace();
            return ResponseEntity.status(e.getStatusCode()).body(new UserResponse(e.getStatusCode(), e.getMessage()));
        }
    }
}