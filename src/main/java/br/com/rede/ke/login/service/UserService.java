/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserService.java
 * Descrição: UserService.java.
 * Autor    : Geisly Conca <gconca@thoughtworks.com>
 * Data     : 26/12/2016
 * Empresa  : Thoughtworks
 */
package br.com.rede.ke.login.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableSet;

import br.com.rede.ke.login.entity.Acquirer;
import br.com.rede.ke.login.entity.Pv;
import br.com.rede.ke.login.entity.User;
import br.com.rede.ke.login.model.PnAuthorization;
import br.com.rede.ke.login.model.PvDTO;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.repository.AcquirerRepository;
import br.com.rede.ke.login.repository.PvRepository;
import br.com.rede.ke.login.repository.UserRepository;

/**
 * Class UserService.
 */
@Component
@Transactional
public class UserService {

    /** The Constant REDE_ACQUIRER. */
    private static final String REDE_ACQUIRER = "REDE";

    /** The pv repository. */
    @Autowired
    private PvRepository pvRepository;

    /** The user repository. */
    @Autowired
    private UserRepository userRepository;

    /** The acquirer repository. */
    @Autowired
    private AcquirerRepository acquirerRepository;

    /**
     * Popula UserDTO com pvs autorizados pelo PN, com pvs da Rede e outros
     * adquirentes.
     *
     * @param pnAuthorization
     *            autorização PN
     * @param pvCodeFilter
     *            pvs filtro
     * @return userDTO com pvs autorizados
     */
    public UserDTO fromPnAuthorization(PnAuthorization pnAuthorization, Collection<String> pvCodeFilter) {

        Acquirer redeAcquirer = acquirerRepository.findByName(REDE_ACQUIRER);
        Stream<Pv> redePvs = pvRepository.findByAcquirerAndCodeIn(
            redeAcquirer, pnAuthorization.getRedePvCodes()).stream();

        Stream<Pv> acquirersNotRedePvs = Optional.ofNullable(userRepository.findByEmail(pnAuthorization.getUserEmail()))
            .map(User::getPvs)
            .map(Collection::stream)
            .orElse(Stream.empty());

        if (isPoynt(pvCodeFilter)) {
            return new UserDTO(pnAuthorization.getUserEmail(), pnAuthorization.getAccessToken(),
                getAuthorizedPvs(pvCodeFilter).apply(redePvs));
        }

        return new UserDTO(pnAuthorization.getUserEmail(), pnAuthorization.getAccessToken(),
            getAuthorizedPvs(null).apply(Stream.concat(redePvs, acquirersNotRedePvs)));
    }

    public boolean saveUser(String email) {
        boolean ret = false;

        if(userRepository.findByEmail(email) == null) {
            User user = new User();
            user.setEmail(email);

            userRepository.save(user);

            ret = true;
        }

        return ret;
    }

    /**
     * Gets the authorized pvs.
     *
     * @param pvPoyntFilter the pv poynt filter
     * @return the authorized pvs
     */
    private Function<Stream<Pv>, List<PvDTO>> getAuthorizedPvs(Collection<String> pvPoyntFilter) {
        return pvStream -> {

            Stream<Pv> pvsWithBranches = pvStream
                .flatMap(getBranchesFromHeadquarters());

            if (pvPoyntFilter != null) {
                final ImmutableSet pvsAllowedForPoynt = ImmutableSet.copyOf(pvPoyntFilter);
                return mapToDistinctDTO().apply(
                    pvsWithBranches.filter(pv -> pvsAllowedForPoynt.contains(pv.getCode())))
                    .collect(Collectors.toList());
            }

            return mapToDistinctDTO().apply(pvsWithBranches).collect(Collectors.toList());
        };
    }

    /**
     * Map to distinct DTO.
     *
     * @return the function
     */
    private Function<Stream<Pv>, Stream<PvDTO>> mapToDistinctDTO() {
        return pvStream -> pvStream
            .map(PvDTO::new)
            .distinct();
    }

    /**
     * Gets the branches from headquarters.
     *
     * @return the branches from headquarters
     */
    private Function<Pv, Stream<? extends Pv>> getBranchesFromHeadquarters() {
        return pv -> {
            if (isHeadquarter(pv)) {
                Set<Pv> branches = getOrReturnEmpty(pv.getBranches());
                return Stream.concat(Stream.of(pv), branches.stream());
            }
            return Stream.of(pv);
        };
    }

    /**
     * Checks if is callcenter.
     *
     * @param pnAuthorization the pn authorization
     * @return true, if is callcenter
     */
    private boolean isCallcenter(PnAuthorization pnAuthorization) {
        return pnAuthorization.hasRestrictedAccess();
    }

    /**
     * Checks if is poynt.
     *
     * @param pvCodeFilter the pv code filter
     * @return true, if is poynt
     */
    private boolean isPoynt(Collection<String> pvCodeFilter) {
        return pvCodeFilter != null && !pvCodeFilter.isEmpty();
    }

    /**
     * Checks if is headquarter.
     *
     * @param pv the pv
     * @return true, if is headquarter
     */
    private boolean isHeadquarter(Pv pv) {
        return pv.getHeadquarter() == null;
    }

    /**
     * Gets the or return empty.
     *
     * @param branches the branches
     * @return the or return empty
     */
    private Set<Pv> getOrReturnEmpty(Set<Pv> branches) {
        return Optional.ofNullable(branches)
            .orElse(Collections.emptySet());
    }
}
