/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserRepository.java
 * Descrição: UserRepository.java.
 * Autor    : Maitê Balhester <mbalhest@thoughtworks.com>
 * Data     : 21/12/2016
 * Empresa  : Thoughtworks
 */
package br.com.rede.ke.login.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rede.ke.login.entity.User;

/**
 * The Interface UserRepository.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

   /**
    * Find by email.
    *
    * @param email the email
    * @return the user
    */
    User findByEmail(String email);

}
