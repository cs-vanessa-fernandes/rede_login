/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : KibanaUserProperties.java
 * Descrição: KibanaUserProperties.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 09/02/2017
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 06/03/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 191300
 *************************************************************
 */
package br.com.rede.ke.login.logger;

/**
 * The Enum KibanaUserProperties.
 */
public enum KibanaUserProperties {
    /** The tag login. */
    TAG_LOGIN("login"),

    /** The tag login user created. */
    TAG_LOGIN_USER_CREATED("login_user_created_upon_login"),

    /** The tag login domain. */
    TAG_LOGIN_DOMAIN("login_domain"),

    /** The tag login type. */
    TAG_LOGIN_TYPE("type"),

    /** The tag pvlist. */
    TAG_PVLIST("pvlist"),

    /** The tag useragent. */
    TAG_USERAGENT("useragent"),

    /** The tag useragent raw. */
    TAG_USERAGENT_RAW("useragent_raw"),

    /** The tag user os. */
    TAG_USER_OS("useragent_os"),

    /** The tag user os. */
    TAG_USER_OS_RAW("useragent_os_raw"),

    /** The tag user os. */
    TAG_USER_OS_NAME("useragent_os_name"),

    /** The tag user os version. */
    TAG_USER_OS_VERSION("useragent_os_version"),

    /** The tag user browser. */
    TAG_USER_BROWSER("useragent_browser"),

    /** The tag user browser. */
    TAG_USER_BROWSER_NAME("useragent_browser_name"),

    /** The tag user browser. */
    TAG_USER_BROWSER_RAW("useragent_browser_raw"),

    /** The tag user browser version. */
    TAG_USER_BROWSER_VERSION("useragent_browser_version"),

    /** The tag user device. */
    TAG_USER_DEVICE("useragent_device"),

    /** The tag clientip. */
    TAG_CLIENTIP("client_ip"),

    /** The tag httpstatuscode. */
    TAG_HTTPSTATUSCODE("http_status_code"),

    /** The tag geolocation. */
    TAG_GEOLOCATION("geoip"),

    /** The tag statusmessage. */
    TAG_HTTPSTATUSMESSAGE("http_status_message");

    /** The tag value. */
    private String tagValue;

    /**
     * Instantiates a new kibana user properties.
     *
     * @param tagValue the tag value
     */
    KibanaUserProperties(String tagValue) {
        this.tagValue = tagValue;
    }

    /**
     * Gets the tag value.
     *
     * @return the tagValue
     */
    public String getTagValue() {
        return tagValue;
    }

    /**
     * Sets the tag value.
     *
     * @param tagValue the tagValue to set
     */
    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }
}