/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PnLoginResponse.java
 * Descrição: PnLoginResponse.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.rede.ke.login.logger.UserTracking;

/**
 * The Class PnLoginResponse.
 */
public class PnLoginResponse {

    @JsonProperty("access_token")
    private String accessToken;

    private List<Entidade> entidades;

    @JsonProperty("status_retorno")
    private StatusRetorno statusRetorno;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonIgnore
    private UserTracking userTracking;

    /**
     * Gets the access token.
     *
     * @return the access token
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Sets the access token.
     *
     * @param accessToken the new access token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * Gets the entidades.
     *
     * @return the entidades
     */
    public List<Entidade> getEntidades() {
        return entidades;
    }

    /**
     * Sets the entidades.
     *
     * @param entidades the new entidades
     */
    public void setEntidades(List<Entidade> entidades) {
        this.entidades = entidades;
    }

    /**
     * Gets the status retorno.
     *
     * @return the status retorno
     */
    public StatusRetorno getStatusRetorno() {
        return statusRetorno;
    }

    /**
     * Sets the status retorno.
     *
     * @param statusRetorno the new status retorno
     */
    public void setStatusRetorno(StatusRetorno statusRetorno) {
        this.statusRetorno = statusRetorno;
    }

    /**
     * Gets the token type.
     *
     * @return the token type
     */
    public String getTokenType() {
        return tokenType;
    }

    /**
     * Sets the token type.
     *
     * @param tokenType the new token type
     */
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    /**
     * @return the userTracking
     */
    public UserTracking getUserTracking() {
        return userTracking;
    }

    /**
     * @param userTracking the userTracking to set
     */
    public void setUserTracking(UserTracking userTracking) {
        this.userTracking = userTracking;
    }
}
