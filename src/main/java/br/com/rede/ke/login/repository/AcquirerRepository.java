/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : AcquirerRepository.java
 * Descrição: AcquirerRepository.java.
 * Autor    : Geisly Conca <gconca@thoughtworks.com>
 * Data     : 26/12/2016
 * Empresa  : Thoughtworks
 */
package br.com.rede.ke.login.repository;

import br.com.rede.ke.login.entity.Acquirer;
import org.springframework.stereotype.Repository;

/**
 * Class AcquirerRepository.
 */
@Repository
public interface AcquirerRepository extends org.springframework.data.repository.Repository<Acquirer, Integer> {
    /**
     * Acha adquirente por nome.
     *
     * @param name nome da adquirente
     * @return a adquirente
     */
    Acquirer findByName(String name);
}
