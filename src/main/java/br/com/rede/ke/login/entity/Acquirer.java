/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : Acquirer.java
 * Descrição: Acquirer.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Acquirer.
 */
@Entity
@Table(name = "ACQUIRER")
public class Acquirer {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * With.
     *
     * @param id the id
     * @return the acquirer
     */
    public static Acquirer with(Integer id) {
        Acquirer acquirer = new Acquirer();
        acquirer.setId(id);
        return acquirer;
    }
}
