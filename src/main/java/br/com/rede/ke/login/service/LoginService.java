/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LoginService.java
 * Descrição: LoginService.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.service;

import java.io.BufferedReader;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.rede.ke.login.config.LoginConfig;
import br.com.rede.ke.login.exception.ConciliationLoginException;
import br.com.rede.ke.login.exception.TokenValidationException;
import br.com.rede.ke.login.logger.UserTracking;
import br.com.rede.ke.login.model.PnAuthorization;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.model.request.PnLoginRequest;
import br.com.rede.ke.login.model.request.UserRequest;
import br.com.rede.ke.login.model.response.PnLoginResponse;
import br.com.rede.ke.login.nosql.TokenRedisRepository;
import br.com.rede.ke.login.util.FileRepository;
import br.com.rede.ke.login.util.HttpUtils;
import br.com.rede.ke.login.util.TokenReader;

/**
 * The Class LoginService.
 */
@Service
public class LoginService {

    /** The logger. */
    private Logger logger = LoggerFactory.getLogger(LoginService.class);

    /** The token redis repository. */
    @Autowired
    private TokenRedisRepository tokenRedisRepository;

    /** The login config. */
    @Autowired
    private LoginConfig loginConfig;

    /** The rest template. */
    @Autowired
    private RestTemplate restTemplate;

    /** The file repository. */
    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private UserService userService;

    /**
     * Inits the.
     *
     * @throws TokenValidationException
     *             the token validation exception
     */
    @PostConstruct
    public void init() throws TokenValidationException {
        TokenReader.initializePublicKey(fileRepository, loginConfig.getConfigurationBucketName());
    }

    /**
     * Login.
     *
     * @param userRequest
     *            the user request
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    public UserDTO login(UserRequest userRequest) throws ConciliationLoginException {
        return doLogin(userRequest, null, "WITH_CREDENTIALS");
    }

    /**
     * Login point.
     *
     * @param userRequest
     *            the user request
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    public UserDTO loginPoint(UserRequest userRequest) throws ConciliationLoginException {
        List<String> pvCodeFilter = Collections
            .singletonList(String.format("%09d", Long.parseLong(userRequest.getPvCode())));
        return doLogin(userRequest, pvCodeFilter, "POYNT");
    }

    /**
     * Do login.
     *
     * @param userRequest
     *            the user request
     * @param pvCodeFilter
     *            the pv code filter
     * @param loginType
     *            the login type
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    private UserDTO doLogin(UserRequest userRequest, List<String> pvCodeFilter, String loginType)
        throws ConciliationLoginException {

        PnLoginRequest pnLoginRequest = PnLoginRequest.from(userRequest, loginConfig, loginType);

        UserTracking userTracking = buildUserTracking(userRequest);

        try {
            PnLoginResponse pnResponse = restTemplate.postForObject(loginConfig.getPnUrl(), pnLoginRequest,
                PnLoginResponse.class);

            pnResponse.setUserTracking(userTracking);

            return handlePnResponse(pnResponse, 200, pvCodeFilter, loginType);
            // CHECKSTYLE.OFF: IllegalCatch - Necessário para tratar exceções
            // adicionais
        } catch (Exception e) {
            userTracking.setStatusMessage("General Exception: " + e.getMessage());
            // CHECKSTYLE.ON: IllegalCatch
            return handleException(e, loginType, userTracking);
        }
    }

    /**
     * Handle exception.
     *
     * @param e
     *            the e
     * @param loginType
     *            the login type
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    private UserDTO handleException(Exception e, String loginType, UserTracking userTracking)
        throws ConciliationLoginException {
        if (e instanceof RestClientResponseException) {
            RestClientResponseException responseException = (RestClientResponseException) e;
            return handlePnError(responseException, loginType, userTracking);
        } else if (e instanceof ConciliationLoginException) {
            ConciliationLoginException loginException = (ConciliationLoginException) e;
            throw loginException;
        } else {
            logger.error(UserTracking.LOG_ERROR, userTracking.toLogValues());

            throw new ConciliationLoginException(ConciliationLoginException.INTERNAL_SERVER_ERROR_MESSAGE, 500, e);
        }
    }

    /**
     * Single signon.
     *
     * @param userRequest
     *            the user request
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    public UserDTO singleSignon(UserRequest userRequest) throws ConciliationLoginException {
        String loginType = "SINGLE_SIGNON";

        UserTracking userTracking = buildUserTracking(userRequest);

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("authorization", userRequest.getAuthorization());

            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<PnLoginResponse> response = restTemplate.exchange(loginConfig.getPnUrl(), HttpMethod.GET,
                entity, PnLoginResponse.class);

            PnLoginResponse pnResponse = response.getBody();
            pnResponse.setUserTracking(userTracking);

            return handlePnResponse(pnResponse, 200, null, loginType);
            // CHECKSTYLE.OFF: IllegalCatch - Necessário para tratar exceções
            // adicionais
        } catch (Exception e) {
            // CHECKSTYLE.ON: IllegalCatch
            return handleException(e, loginType, userTracking);
        }
    }

    /**
     * Login mock.
     *
     * @param userRequest
     *            the user request
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    public UserDTO loginMock(UserRequest userRequest) throws ConciliationLoginException {
        String loginType = "MOCK";
        loginConfig.ensureMockEnabled();
        UserTracking userTracking = buildUserTracking(userRequest);

        ObjectMapper mapper = new ObjectMapper();
        String bucket = loginConfig.getConfigurationBucketName();
        String key = "elbinstance/pn_mock_response.json";
        try (BufferedReader bufferReader = fileRepository.getBufferReader(bucket, key)) {
            PnLoginResponse pnLoginResponse = mapper.readValue(bufferReader, PnLoginResponse.class);
            pnLoginResponse.setUserTracking(userTracking);
            return handlePnResponse(pnLoginResponse, 200, null, loginType);
            // CHECKSTYLE.OFF: IllegalCatch - Necessário para tratar exceções
            // adicionais
        } catch (Exception e) {
            // CHECKSTYLE.ON: IllegalCatch
            return handleException(e, loginType, userTracking);
        }
    }

    /**
     * Handle pn error.
     *
     * @param e
     *            the e
     * @param loginType
     *            the login type
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    private UserDTO handlePnError(RestClientResponseException e, String loginType, UserTracking userTracking)
        throws ConciliationLoginException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            PnLoginResponse pnLoginResponse = mapper.readValue(e.getResponseBodyAsString(), PnLoginResponse.class);
            pnLoginResponse.setUserTracking(userTracking);
            Integer statusCode = e.getRawStatusCode();
            return handlePnResponse(pnLoginResponse, statusCode, null, loginType);
        } catch (ConciliationLoginException ex) {
            throw ex;
            // CHECKSTYLE.OFF: IllegalCatch - Necessário para tratar exceções
            // adicionais
        } catch (Exception ex) {
            // CHECKSTYLE.ON: IllegalCatch
            throw new IllegalStateException();
        }
    }

    /**
     * Handle pn response.
     *
     * @param pnLoginResponse
     *            the pn login response
     * @param statusCode
     *            the status code
     * @param pvCodeFilter
     *            the pv code filter
     * @param loginType
     *            the login type
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    private UserDTO handlePnResponse(PnLoginResponse pnLoginResponse, Integer statusCode,
        Collection<String> pvCodeFilter, String loginType) throws ConciliationLoginException {

        UserTracking userTracking = pnLoginResponse.getUserTracking();
        userTracking.setStatusCode(statusCode);

        if (statusCode >= 200 && statusCode < 300) {
            return handlePnResponseOk(pnLoginResponse, pvCodeFilter, loginType);
        } else {
            if (statusCode == 403) {
                if (pnLoginResponse.getStatusRetorno().getCodigo().equals("1103")) {
                    userTracking.setStatusMessage("User is blocked");
                    logger.error(UserTracking.LOG_ERROR, userTracking.toLogValues());
                    throw new ConciliationLoginException(ConciliationLoginException.USER_BLOCKED_MESSAGE, 401);
                } else {
                    userTracking.setStatusMessage("User not authorized by PN");
                    logger.error(UserTracking.LOG_ERROR, userTracking.toLogValues());

                    throw new ConciliationLoginException(ConciliationLoginException.UNAUTHORIZED_MESSAGE, 401);
                }
            }

            userTracking.setStatusMessage("Unexpected error on PN Response");
            logger.error(UserTracking.LOG_ERROR, userTracking.toLogValues());

            throw new ConciliationLoginException(ConciliationLoginException.INTERNAL_SERVER_ERROR_MESSAGE, 500);
        }
    }

    /**
     * Handle pn response ok.
     *
     * @param pnLoginResponse
     *            the pn login response
     * @param pvCodeFilter
     *            the pv code filter
     * @param loginType
     *            the login type
     * @return the user
     * @throws ConciliationLoginException
     *             the conciliation login exception
     */
    private UserDTO handlePnResponseOk(PnLoginResponse pnLoginResponse, Collection<String> pvCodeFilter,
        String loginType) throws ConciliationLoginException {

        try {
            UserTracking userTracking = pnLoginResponse.getUserTracking();

            TokenReader tokenReader = new TokenReader(pnLoginResponse.getAccessToken()).parse();
            PnAuthorization authorization = new PnAuthorization(pnLoginResponse, tokenReader);
            UserDTO userDTO = userService.fromPnAuthorization(authorization, pvCodeFilter);

            if (userDTO.getPvList().isEmpty()) {
                userTracking.setUser(userDTO);
                userTracking.setStatusMessage("No PVs associated with current user");
                logger.warn(UserTracking.LOG_WARN, userTracking.toLogValues());

                throw new ConciliationLoginException(ConciliationLoginException.UNAUTHORIZED_MESSAGE, 401);
            }

            Long expiration = loginConfig.getTokenExpireTime();
            if (authorization.hasRestrictedAccess()) {
                expiration = loginConfig.getRestrictedUserTokenExpireTime();
            }

            tokenRedisRepository.setPvList(pnLoginResponse.getAccessToken(), userDTO.getPvList(), expiration);

            // Everything is fine, access granted
            userTracking.setCreatedUponLogon(userService.saveUser(userDTO.getLogin()));

            userTracking.setUser(userDTO);
            logger.info(UserTracking.LOG_OK, userTracking.toLogValues());

            return userDTO;
        } catch (TokenValidationException e) {
            throw new ConciliationLoginException(ConciliationLoginException.UNAUTHORIZED_MESSAGE, 401, e);
        }
    }

    private UserTracking buildUserTracking(UserRequest userRequest) {
        String clientIPAddress = HttpUtils.getClientIPAddress(userRequest.getRequest());

        UserTracking userTracking = new UserTracking(userRequest.getLogin(), userRequest.getRequest(), clientIPAddress);
        userTracking.setLoginType(userRequest.getLoginType());

        return userTracking;
    }
}
