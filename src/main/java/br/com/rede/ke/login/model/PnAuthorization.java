/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PnAuthorization.java
 * Descrição: PnAuthorization.java.
 * Autor    : Mariana Bravo <mbravo@thoughtworks.com>
 * Data     : 23/12/2016
 * Empresa  : ThoughtWorks
 */
package br.com.rede.ke.login.model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import br.com.rede.ke.login.model.response.PnLoginResponse;
import br.com.rede.ke.login.util.TokenReader;

/**
 * The Class PnAuthorization.
 */
public class PnAuthorization {

    /** The pn login response. */
    private PnLoginResponse pnLoginResponse;

    /** The token reader. */
    private TokenReader tokenReader;

    /**
     * Instantiates a new pn authorization.
     *
     * @param pnLoginResponse
     *            the pn login response
     * @param tokenReader
     *            the token reader
     */
    public PnAuthorization(PnLoginResponse pnLoginResponse, TokenReader tokenReader) {
        this.pnLoginResponse = pnLoginResponse;
        this.tokenReader = tokenReader;
    }

    /**
     * Gets the access token.
     *
     * @return the access token
     */
    public String getAccessToken() {
        return pnLoginResponse.getAccessToken();
    }

    /**
     * Gets the Rede pv codes.
     *
     * @return the pv codes
     */
    public List<String> getRedePvCodes() {
        return pnLoginResponse.getEntidades().stream()
            .map(entidade -> StringUtils.leftPad(entidade.getCodigo(), 9, "0"))
            .collect(Collectors.toList());
    }

    /**
     * Checks for restricted access.
     *
     * @return true, if successful
     */
    public boolean hasRestrictedAccess() {
        Map<?, ?> user = tokenReader.getClaim("usuario", Map.class);

        if (user.containsKey("Atendimento") && user.get("Atendimento") != null) {
            return (Boolean) user.get("Atendimento");
        }

        return false;
    }

    public String getUserEmail() {
        Map<?, ?> user = tokenReader.getClaim("usuario", Map.class);
        return (String) user.get("Email");
    }
}
