/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PnLoginRequest.java
 * Descrição: PnLoginRequest.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.request;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.rede.ke.login.config.LoginConfig;
import br.com.rede.ke.login.exception.ConciliationLoginException;
import br.com.rede.ke.login.logger.UserTracking;

// TODO: Auto-generated Javadoc
/**
 * The Class PnLoginRequest.
 */
public class PnLoginRequest {

    private String usuario;
    private String senha;

    @JsonProperty("grupo_entidade")
    private String grupoEntidade;

    @JsonProperty("codigo_cliente")
    private String codigoCliente;

    /**
     * Gets the usuario.
     *
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Sets the usuario.
     *
     * @param usuario the new usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * Gets the senha.
     *
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Sets the senha.
     *
     * @param senha the new senha
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * Gets the grupo entidade.
     *
     * @return the grupo entidade
     */
    public String getGrupoEntidade() {
        return grupoEntidade;
    }

    /**
     * Sets the grupo entidade.
     *
     * @param grupoEntidade the new grupo entidade
     */
    public void setGrupoEntidade(String grupoEntidade) {
        this.grupoEntidade = grupoEntidade;
    }

    /**
     * Gets the codigo cliente.
     *
     * @return the codigo cliente
     */
    public String getCodigoCliente() {
        return codigoCliente;
    }

    /**
     * Sets the codigo cliente.
     *
     * @param codigoCliente the new codigo cliente
     */
    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    /**
     * From.
     *
     * @param request the request
     * @param config the config
     * @param loginType the login type
     * @return the pn login request
     * @throws ConciliationLoginException the conciliation login exception
     */
    public static PnLoginRequest from(UserRequest request, LoginConfig config, String loginType)
        throws ConciliationLoginException {

        Logger logger = LoggerFactory.getLogger(PnLoginRequest.class);

        if (StringUtils.isBlank(request.getLogin()) || StringUtils.isBlank(request.getPassword())) {
            logger.error(UserTracking.LOG_WARN, "User Name or Password is null");
            throw new ConciliationLoginException(ConciliationLoginException.WRONG_CREDENTIALS_MESSAGE, 400);
        }

        PnLoginRequest pnRequest = new PnLoginRequest();

        pnRequest.usuario = request.getLogin();
        pnRequest.senha = request.getPassword();
        pnRequest.codigoCliente = config.getPnClientCode();
        pnRequest.grupoEntidade = config.getPnEntityGroup();

        return pnRequest;
    }
}
