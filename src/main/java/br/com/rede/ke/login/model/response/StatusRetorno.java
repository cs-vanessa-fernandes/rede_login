/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : StatusRetorno.java
 * Descrição: StatusRetorno.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.login.model.response;

// TODO: Auto-generated Javadoc
/**
 * The Class StatusRetorno.
 */
public class StatusRetorno {

    private String codigo;
    private String mensagem;

    /**
     * Gets the codigo.
     *
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the codigo.
     *
     * @param codigo the new codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Gets the mensagem.
     *
     * @return the mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Sets the mensagem.
     *
     * @param mensagem the new mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
