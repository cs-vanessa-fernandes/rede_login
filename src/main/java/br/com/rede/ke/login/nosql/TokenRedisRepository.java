/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : TokenRedisRepository.java
 * Descrição: TokenRedisRepository.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.login.nosql;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.rede.ke.login.model.PvDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenRedisRepository.
 */
@Repository
public class TokenRedisRepository {

    @Autowired
    private TokenRedisTemplate redisTemplate;

    /**
     * Sets the pv list.
     *
     * @param token the token
     * @param pvs the pvs
     * @param expirationInSeconds the expiration in seconds
     */
    public void setPvList(String token, List<PvDTO> pvs, Long expirationInSeconds) {
        if (token != null && !token.trim().isEmpty() && pvs != null && pvs.size() > 0) {
            String key = "token:" + token;

            redisTemplate.opsForValue().set(key, pvs);
            redisTemplate.expire(key, expirationInSeconds, TimeUnit.SECONDS);
        }
    }
}