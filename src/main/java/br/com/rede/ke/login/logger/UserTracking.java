/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : UserTracking.java
 * Descrição: UserTracking.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 09/02/2017
 * Empresa  : Rede
 */
package br.com.rede.ke.login.logger;

import static net.logstash.logback.argument.StructuredArguments.value;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.rede.ke.login.Types;
import br.com.rede.ke.login.model.UserDTO;
import br.com.rede.ke.login.util.HttpUtils;
import net.logstash.logback.argument.StructuredArgument;

/**
 * The Class UserTracking.
 */
public class UserTracking {

    /** The Constant LOG_OK. */
    public static final String LOG_OK = "LOGIN_OK {}";

    /** The Constant LOG_WARN. */
    public static final String LOG_WARN = "LOGIN_WARN {}";

    /** The Constant LOG_ERROR. */
    public static final String LOG_ERROR = "LOGIN_ERROR {}";

    /** The user. */
    private UserDTO user;

    /** The request. */
    private HttpServletRequest request;

    /** The client ip. */
    private String clientIp;

    /** The status code. */
    private int statusCode;

    /** The message. */
    private String statusMessage;

    /** The login type. */
    private Types loginType;

    /** The created upon logon. */
    private boolean createdUponLogon;

    /**
     * Instantiates a new user tracking.
     *
     * @param request the request
     * @param clientIp            the client ip
     */
    public UserTracking(HttpServletRequest request, String clientIp) {
        this.request = request;
        this.clientIp = clientIp;
    }

    /**
     * Instantiates a new user tracking.
     *
     * @param user            the user
     * @param request the request
     * @param clientIp            the client ip
     */
    public UserTracking(UserDTO user, HttpServletRequest request, String clientIp) {
        this.user = user;
        this.request = request;
        this.clientIp = clientIp;
    }

    /**
     * Instantiates a new user tracking.
     *
     * @param login            the login
     * @param request the request
     * @param clientIp            the client ip
     */
    public UserTracking(String login, HttpServletRequest request, String clientIp) {
        if (user == null) {
            user = new UserDTO();
        }

        user.setLogin(login);
        this.request = request;
        this.clientIp = clientIp;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public UserDTO getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user
     *            the user to set
     */
    public void setUser(UserDTO user) {
        this.user = user;
    }

    /**
     * Gets the client ip.
     *
     * @return the clientIp
     */
    public String getClientIp() {
        return clientIp;
    }

    /**
     * Sets the client ip.
     *
     * @param clientIp
     *            the clientIp to set
     */
    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    /**
     * Gets the status code.
     *
     * @return the statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the status code.
     *
     * @param statusCode
     *            the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Gets the status message.
     *
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Sets the status message.
     *
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * Gets the login type.
     *
     * @return the loginType
     */
    public Types getLoginType() {
        return loginType;
    }

    /**
     * Sets the login type.
     *
     * @param loginType the loginType to set
     */
    public void setLoginType(Types loginType) {
        this.loginType = loginType;
    }

    /**
     * To json.
     *
     * @return the string
     */
    public String toJson() {
        String json = "";

        try {
            json = new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Gets the request.
     *
     * @return the request
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * Sets the request.
     *
     * @param request the request to set
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Checks if is created upon logon.
     *
     * @return the createdAtLogon
     */
    public boolean isCreatedUponLogon() {
        return createdUponLogon;
    }

    /**
     * Sets the created upon logon.
     *
     * @param createdUponLogon the new created upon logon
     */
    public void setCreatedUponLogon(boolean createdUponLogon) {
        this.createdUponLogon = createdUponLogon;
    }

    /**
     * To log values.
     *
     * @return the object[]
     */
    public Object[] toLogValues() {
        String userAgent = HttpUtils.getUserAgent(request);

        String loginEmailDomain = "";

        if(user.getLogin() != null && user.getLogin().contains("@")) {
            String loginEmail = user.getLogin();
            String[] loginEmailSplitted = loginEmail.split("@");

            if(loginEmailSplitted.length > 0) {
                loginEmailDomain = loginEmailSplitted[1];
            }
        }

        return new StructuredArgument[] {
            value(KibanaUserProperties.TAG_LOGIN.getTagValue(), user.getLogin()),
            value(KibanaUserProperties.TAG_LOGIN_USER_CREATED.getTagValue(), createdUponLogon),
            value(KibanaUserProperties.TAG_LOGIN_DOMAIN.getTagValue(), loginEmailDomain),
            value(KibanaUserProperties.TAG_LOGIN_TYPE.getTagValue(), loginType),
            value(KibanaUserProperties.TAG_PVLIST.getTagValue(), user.getPvList()),
            value(KibanaUserProperties.TAG_USERAGENT.getTagValue(), userAgent),
            value(KibanaUserProperties.TAG_USERAGENT_RAW.getTagValue(), userAgent),
            value(KibanaUserProperties.TAG_USER_OS.getTagValue(), HttpUtils.getUserOs(userAgent)),
            value(KibanaUserProperties.TAG_USER_OS_RAW.getTagValue(), HttpUtils.getUserOs(userAgent)),
            value(KibanaUserProperties.TAG_USER_OS_NAME.getTagValue(), HttpUtils.getUserOsName(userAgent)),
            value(KibanaUserProperties.TAG_USER_OS_VERSION.getTagValue(), HttpUtils.getUserOsVersion(userAgent)),
            value(KibanaUserProperties.TAG_USER_BROWSER.getTagValue(), HttpUtils.getUserBrowser(userAgent)),
            value(KibanaUserProperties.TAG_USER_BROWSER_RAW.getTagValue(), HttpUtils.getUserBrowser(userAgent)),
            value(KibanaUserProperties.TAG_USER_BROWSER_NAME.getTagValue(), HttpUtils.getUserBrowserName(userAgent)),
            value(KibanaUserProperties.TAG_USER_BROWSER_VERSION.getTagValue(), HttpUtils.getUserBrowserVersion(userAgent)),
            value(KibanaUserProperties.TAG_GEOLOCATION.getTagValue(), HttpUtils.getUserGeoLocation(request)),
            value(KibanaUserProperties.TAG_CLIENTIP.getTagValue(), clientIp),
            value(KibanaUserProperties.TAG_HTTPSTATUSCODE.getTagValue(), String.valueOf(statusCode)),
            value(KibanaUserProperties.TAG_HTTPSTATUSMESSAGE.getTagValue(), statusMessage) };
    }
}