/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : Types.java
 * Descrição: Types.java.
 * Autor    : Gustavo Silva <gustavo.silva@iteris.com.br>
 * Data     : 22/02/2017
 * Empresa  : Iteris
 */
package br.com.rede.ke.login;

public enum Types {
    SINGLE_SIGNON,
    LOGIN,
    POYNT,
    MOCK;
}
