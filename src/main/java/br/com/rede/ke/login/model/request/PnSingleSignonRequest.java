/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : PnSingleSignonRequest.java
 * Descrição: PnSingleSignonRequest.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.request;

import br.com.rede.ke.login.config.LoginConfig;
import br.com.rede.ke.login.exception.ConciliationLoginException;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class PnSingleSignonRequest.
 */
public class PnSingleSignonRequest {
    @JsonProperty("token")
    private String authorization;

    /**
     * From.
     *
     * @param request the request
     * @param config the config
     * @return the pn single signon request
     * @throws ConciliationLoginException the conciliation login exception
     */
    public static PnSingleSignonRequest from(UserRequest request, LoginConfig config)
        throws ConciliationLoginException {

        if (request.getAuthorization() == null) {
            throw new ConciliationLoginException(ConciliationLoginException.WRONG_AUTHORIZATION_MESSAGE, 400);
        }

        PnSingleSignonRequest pnRequest = new PnSingleSignonRequest();

        pnRequest.authorization = request.getAuthorization();

        return pnRequest;
    }

    /**
     * Gets the authorization.
     *
     * @return the authorization
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Sets the authorization.
     *
     * @param authorization the authorization to set
     */
    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}