/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : FileRepository.java
 * Descrição: FileRepository.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.util;

import java.io.BufferedReader;
import java.io.InputStream;

import br.com.rede.ke.login.exception.FileRepositoryException;

/**
 * The Interface FileRepository.
 */
public interface FileRepository {

    /**
     * Cut file.
     *
     * @param bucketNameSource the bucket name source
     * @param pathSource the path source
     * @param bucketNameTarget the bucket name target
     * @param pathTarget the path target
     * @throws FileRepositoryException the file repository exception
     */
    void cutFile(String bucketNameSource, String pathSource, String bucketNameTarget, String pathTarget)
        throws FileRepositoryException;

    /**
     * Copy file.
     *
     * @param bucketNameSource the bucket name source
     * @param pathSource the path source
     * @param bucketNameTarget the bucket name target
     * @param pathTarget the path target
     * @throws FileRepositoryException the file repository exception
     */
    void copyFile(String bucketNameSource, String pathSource, String bucketNameTarget, String pathTarget)
        throws FileRepositoryException;

    /**
     * Up load file.
     *
     * @param bucketName the bucket name
     * @param input the input
     * @param path the path
     * @return the int
     * @throws FileRepositoryException the file repository exception
     */
    int upLoadFile(String bucketName, InputStream input, String path) throws FileRepositoryException;

    /**
     * Copy file S 3.
     *
     * @param bucketNameSource the bucket name source
     * @param sourceKey the source key
     * @param bucketNameDestiny the bucket name destiny
     * @param destinationKey the destination key
     * @throws FileRepositoryException the file repository exception
     */
    void copyFileS3(String bucketNameSource, String sourceKey, String bucketNameDestiny, String destinationKey)
        throws FileRepositoryException;

    /**
     * Gets the buffer reader.
     *
     * @param bucket the bucket
     * @param path the path
     * @param fileName the file name
     * @return the buffer reader
     * @throws FileRepositoryException the file repository exception
     */
    BufferedReader getBufferReader(String bucket, String path, String fileName) throws FileRepositoryException;

    /**
     * Gets the buffer reader.
     *
     * @param bucket the bucket
     * @param key the key
     * @return the buffer reader
     * @throws FileRepositoryException the file repository exception
     */
    BufferedReader getBufferReader(String bucket, String key) throws FileRepositoryException;
}
