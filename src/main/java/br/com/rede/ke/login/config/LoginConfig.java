/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : LoginConfig.java
 * Descrição: LoginConfig.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 06/03/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 191300
 *************************************************************
 */
package br.com.rede.ke.login.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import br.com.rede.ke.login.exception.ConciliationLoginException;

/**
 * The Class LoginConfig.
 */
@ConfigurationProperties("login")
public class LoginConfig {

    private long tokenExpireTime;
    private String pnUrl;
    private String pnEntityGroup;
    private String pnClientCode;
    private boolean mockEnabled;
    private String alias;
    private Long restrictedUserTokenExpireTime;

    // Proxy
    private boolean proxyEnabled;
    private String proxyHost;
    private Integer proxyPort;

    /**
     * Gets the token expire time.
     *
     * @return the token expire time
     */
    public long getTokenExpireTime() {
        return tokenExpireTime;
    }

    /**
     * Sets the token expire time.
     *
     * @param tokenExpireTime the new token expire time
     */
    public void setTokenExpireTime(long tokenExpireTime) {
        this.tokenExpireTime = tokenExpireTime;
    }

    /**
     * Gets the pn url.
     *
     * @return the pn url
     */
    public String getPnUrl() {
        return pnUrl;
    }

    /**
     * Sets the pn url.
     *
     * @param pnUrl the new pn url
     */
    public void setPnUrl(String pnUrl) {
        this.pnUrl = pnUrl;
    }

    /**
     * Gets the pn entity group.
     *
     * @return the pn entity group
     */
    public String getPnEntityGroup() {
        return pnEntityGroup;
    }

    /**
     * Sets the pn entity group.
     *
     * @param pnEntityGroup the new pn entity group
     */
    public void setPnEntityGroup(String pnEntityGroup) {
        this.pnEntityGroup = pnEntityGroup;
    }

    /**
     * Gets the pn client code.
     *
     * @return the pn client code
     */
    public String getPnClientCode() {
        return pnClientCode;
    }

    /**
     * Sets the pn client code.
     *
     * @param pnClientCode the new pn client code
     */
    public void setPnClientCode(String pnClientCode) {
        this.pnClientCode = pnClientCode;
    }

    /**
     * Checks if is mock enabled.
     *
     * @return true, if is mock enabled
     */
    public boolean isMockEnabled() {
        return mockEnabled;
    }

    /**
     * Sets the mock enabled.
     *
     * @param mockEnabled the new mock enabled
     */
    public void setMockEnabled(boolean mockEnabled) {
        this.mockEnabled = mockEnabled;
    }

    /**
     * Gets the alias.
     *
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the alias.
     *
     * @param alias the new alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Long getRestrictedUserTokenExpireTime() {
        return restrictedUserTokenExpireTime;
    }

    public void setRestrictedUserTokenExpireTime(Long restrictedUserTokenExpireTime) {
        this.restrictedUserTokenExpireTime = restrictedUserTokenExpireTime;
    }

    /**
     * Gets the configuration bucket name.
     *
     * @return the configuration bucket name
     */
    public String getConfigurationBucketName() {
        String aliasIndicator = alias.toLowerCase();
        return aliasIndicator + "-ctrlrd-environment-configuration";
    }

    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public void setProxyEnabled(boolean proxyEnabled) {
        this.proxyEnabled = proxyEnabled;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(Integer proxyPort) {
        this.proxyPort = proxyPort;
    }

    /**
     * Ensure mock enabled.
     *
     * @throws ConciliationLoginException the conciliation login exception
     */
    public void ensureMockEnabled() throws ConciliationLoginException {
        if (!mockEnabled) {
            throw new ConciliationLoginException("", 404);
        }
    }
}
