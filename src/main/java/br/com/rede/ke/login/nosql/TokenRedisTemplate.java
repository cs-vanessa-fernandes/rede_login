/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : TokenRedisTemplate.java
 * Descrição: TokenRedisTemplate.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 */
package br.com.rede.ke.login.nosql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import br.com.rede.ke.login.model.PvDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenRedisTemplate.
 */
@Component
public class TokenRedisTemplate extends RedisTemplate<String, List<PvDTO>> {

    /**
     * Instantiates a new token redis template.
     *
     * @param connectionFactory the connection factory
     */
    @Autowired
    public TokenRedisTemplate(RedisConnectionFactory connectionFactory) {
        ObjectMapper mapper = new ObjectMapper();
        CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, PvDTO.class);
        this.setKeySerializer(new StringRedisSerializer());
        this.setValueSerializer(new Jackson2JsonRedisSerializer(javaType));
        this.setConnectionFactory(connectionFactory);
    }
}
