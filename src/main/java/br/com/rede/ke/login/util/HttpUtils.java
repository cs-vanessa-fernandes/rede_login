/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : HttpUtils.java
 * Descrição: HttpUtils.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 09/02/2017
 * Empresa  : Rede
 */
package br.com.rede.ke.login.util;

import javax.servlet.http.HttpServletRequest;

import net.sf.uadetector.OperatingSystem;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

/**
 * The Class HttpUtils.
 */
public class HttpUtils {
    /**
     * Gets the client IP address.
     *
     * @param request the request
     * @return the client IP address
     */
    public static String getClientIPAddress(HttpServletRequest request) {
        String ipAddress = "";

        if (request != null) {
            ipAddress = request.getHeader("X-FORWARDED-FOR");

            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
        }

        return ipAddress;
    }

    /**
     * Gets the user agent.
     *
     * @param request the request
     * @return the user agent
     */
    public static String getUserAgent(HttpServletRequest request) {
        String userAgent = "";

        if (request != null) {
            userAgent = request.getHeader("User-Agent");
        }

        return userAgent;
    }

    /**
     * Gets the user os.
     *
     * @param userAgent the user agent
     * @return the user os
     */
    public static String getUserOs(String userAgent) {
        String userOs = "Unknown";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                if(isPoynt(userAgent)) {
                    userOs = "POYNT Mobile";
                } else {
                    OperatingSystem os = agent.getOperatingSystem();

                    userOs = String.format("%s %s", os.getName(), os.getVersionNumber().toVersionString());
                }
            }
        }

        return userOs;
    }

    /**
     * Gets the user os.
     *
     * @param userAgent the user agent
     * @return the user os
     */
    public static String getUserOsName(String userAgent) {
        String userOs = "Unknown";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                if(isPoynt(userAgent)) {
                    userOs = "POYNT Mobile";
                } else {
                    OperatingSystem os = agent.getOperatingSystem();

                    if(os != null) {
                        userOs = os.getName();
                    }
                }
            }
        }

        return userOs;
    }

    /**
     * Gets the user os version.
     *
     * @param userAgent the user agent
     * @return the user os version
     */
    public static String getUserOsVersion(String userAgent) {
        String userOs = "";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                OperatingSystem os = agent.getOperatingSystem();

                if(os != null) {
                    userOs = os.getVersionNumber().toVersionString();
                }
            }
        }

        return userOs;
    }

    /**
     * Gets the user browser.
     *
     * @param userAgent the user agent
     * @return the user browser
     */
    public static String getUserBrowser(String userAgent) {
        String userBrowser = "Unknown";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                if(isPoynt(userAgent)) {
                    userBrowser = "POYNT Mobile App";
                } else {
                    userBrowser = String.format( "%s %s", agent.getName(), agent.getVersionNumber().toVersionString());
                }
            }
        }

        return userBrowser;
    }

    /**
     * Gets the user browser name.
     *
     * @param userAgent the user agent
     * @return the user browser name
     */
    public static String getUserBrowserName(String userAgent) {
        String userBrowser = "Unknown";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                if(isPoynt(userAgent)) {
                    userBrowser = "POYNT Mobile App";
                } else {
                    userBrowser = agent.getName();
                }
            }
        }

        return userBrowser;
    }

    /**
     * Gets the user browser version.
     *
     * @param userAgent the user agent
     * @return the user browser version
     */
    public static String getUserBrowserVersion(String userAgent) {
        String userBrowser = "";

        if(!userAgent.isEmpty()) {
            ReadableUserAgent agent = getReadableUserAgent(userAgent);

            if(agent != null) {
                userBrowser = agent.getVersionNumber().toVersionString();
            }
        }

        return userBrowser;
    }

    /**
     * Gets the user geo location.
     *
     * @param request the request
     * @return the user geo location
     */
    public static String getUserGeoLocation(HttpServletRequest request) {
        String location = "Unknown";

        if(request != null) {
            location = request.getHeader("CloudFront-Viewer-Country");
        }

        return location;
    }

    /**
     * Checks if is poynt.
     *
     * @param userAgent the user agent
     * @return true, if is poynt
     */
    public static boolean isPoynt(String userAgent) {
        return userAgent != null && userAgent.contains("poynt-app-mobile");
    }

    /**
     * Gets the readable user agent.
     *
     * @param userAgent the user agent
     * @return the readable user agent
     */
    private static ReadableUserAgent getReadableUserAgent(String userAgent) {
        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent agent = parser.parse(userAgent);

        return agent;
    }
}