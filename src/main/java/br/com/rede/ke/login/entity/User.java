/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : User.java
 * Descrição: User.java.
 * Autor    : Maitê Balhester <mbalhest@thoughtworks.com>
 * Data     : 20/12/2016
 * Empresa  : Thoughtworks
 */
package br.com.rede.ke.login.entity;

import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * The Class User.
 */
@Entity(name = "USER")
@Table(uniqueConstraints = {@UniqueConstraint(name = "EMAIL_UNIQUE", columnNames = {"EMAIL"})})
public class User {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_PV", joinColumns = {
        @JoinColumn(name = "USER_ID", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_USER"))},
        inverseJoinColumns = {
        @JoinColumn(name = "PV_ID", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_PV"))})
    private Set<Pv> pvs;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the pvs.
     *
     * @return the pvs
     */
    public Set<Pv> getPvs() {
        return pvs;
    }

    /**
     * Sets the pvs.
     *
     * @param pvs
     *            the new pvs
     */
    public void setPvs(Set<Pv> pvs) {
        this.pvs = pvs;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(email, id, pvs);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        User user = (User) obj;
        return Objects.equals(id, user.id)
            && Objects.equals(email, user.email)
            && Objects.equals(pvs, user.pvs);

    }

}
