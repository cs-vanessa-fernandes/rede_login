/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : Pv.java
 * Descrição: Pv.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Geisly Conca <gconca@thoughtworks.com>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Ajuste no estilo do código
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.entity;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * The Class Pv.
 */
@Entity(name = "PV")
public class Pv {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "HEADQUARTER_ID")
    private Pv headquarter;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ACQUIRER_ID")
    private Acquirer acquirer;

    @OneToMany(mappedBy = "headquarter")
    private Set<Pv> branches;

    /**
     * From.
     *
     * @param id the id
     * @return the pv
     */
    public static Pv from(Number id) {
        Pv pv = new Pv();
        pv.id = id.longValue();
        return pv;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the headquarter.
     *
     * @return the headquarter
     */
    public Pv getHeadquarter() {
        return headquarter;
    }

    /**
     * Sets the headquarter.
     *
     * @param headquarter the new headquarter
     */
    public void setHeadquarter(Pv headquarter) {
        this.headquarter = headquarter;
    }

    /**
     * Gets the acquirer.
     *
     * @return the acquirer
     */
    public Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the acquirer.
     *
     * @param acquirer the new acquirer
     */
    public void setAcquirer(Acquirer acquirer) {
        this.acquirer = acquirer;
    }

    /**
     * Gets the branches.
     *
     * @return the branches
     */
    public Set<Pv> getBranches() {
        return branches;
    }

    /**
     * Sets the branches.
     *
     * @param branches the new branches
     */
    public void setBranches(Set<Pv> branches) {
        this.branches = branches;
    }

    /**
     * Checks if is headquarter.
     *
     * @return true, if is headquarter
     */
    public boolean isHeadquarter() {
        return headquarter == null;
    }

    /**
     * With.
     *
     * @param id the id
     * @return the pv
     */
    public static Pv with(Long id) {
        Pv pv = new Pv();
        pv.setId(id);
        return pv;
    }

    /**
     * With.
     *
     * @param acquirer the acquirer
     * @param headquarter the headquarter
     * @param code the code
     * @return the pv
     */
    public static Pv with(Acquirer acquirer, Pv headquarter, String code) {
        Pv pv = new Pv();
        pv.setAcquirer(acquirer);
        pv.setHeadquarter(headquarter);
        pv.setCode(code);
        return pv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)  {
            return true;
        }
        if (o == null || getClass() != o.getClass())  {
            return false;
        }

        Pv pv = (Pv) o;

        return id.equals(pv.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
