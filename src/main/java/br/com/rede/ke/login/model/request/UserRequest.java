/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : UserRequest.java
 * Descrição: UserRequest.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Thoughtworks
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.model.request;

import javax.servlet.http.HttpServletRequest;

import br.com.rede.ke.login.Types;

/**
 * The Class UserRequest.
 */
public class UserRequest {
    private String login;
    private String password;
    private String authorization;
    private String pvCode;
    private Types loginType;

    private HttpServletRequest request;

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login.
     *
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the authorization.
     *
     * @return the authorization
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Gets the pv code.
     *
     * @return the pv code
     */
    public String getPvCode() {
        return pvCode;
    }

    /**
     * Sets the pv code.
     *
     * @param pvCode the new pv code
     */
    public void setPvCode(String pvCode) {
        this.pvCode = pvCode;
    }

    /**
     * Sets the authorization.
     *
     * @param authorization the authorization to set
     */
    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    /**
     * From.
     *
     * @param login the login
     * @param password the password
     * @param authorization the authorization
     * @return the user request
     */
    public static UserRequest from(String login, String password, String authorization) {
        return from(login, password, authorization, null);
    }

    /**
     * From.
     *
     * @param login the login
     * @param password the password
     * @param authorization the authorization
     * @param pvCode the pv code
     * @return the user request
     */
    public static UserRequest from(String login, String password, String authorization, String pvCode) {
        UserRequest request = new UserRequest();

        request.setLogin(login);
        request.setPassword(password);
        request.setAuthorization(authorization);
        request.setPvCode(pvCode);

        return request;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserRequest that = (UserRequest) o;

        if (!login.equals(that.login)) {
            return false;
        }
        return password.equals(that.password);

    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    /**
     * @return the request
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * @return the loginType
     */
    public Types getLoginType() {
        return loginType;
    }

    /**
     * @param loginType the loginType to set
     */
    public void setLoginType(Types loginType) {
        this.loginType = loginType;
    }
}