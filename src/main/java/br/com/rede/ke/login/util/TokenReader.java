/*
 * Copyright 2017 Rede S.A.
 *************************************************************
 * Nome     : TokenReader.java
 * Descrição: TokenReader.java.
 * Autor    : Jefferson Brito <jose.josue@userede.com.br>
 * Data     : 09/02/2017
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Jefferson Brito <jose.josue@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Geração de logs para CloudWatch
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.security.Security;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.springframework.util.Assert;

import br.com.rede.ke.login.exception.FileRepositoryException;
import br.com.rede.ke.login.exception.TokenValidationException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

/**
 * The Class TokenReader.
 */
public class TokenReader {

    /** The Constant PUBLIC_KEY_LOCATION. */
    public static final String PUBLIC_KEY_LOCATION = "lambda/pn_public_key.rsa";

    /** The public key. */
    private static RSAPublicKey publicKey;

    /** The token. */
    private final String token;

    /** The claims. */
    private Jws<Claims> claims;

    /**
     * Instantiates a new token reader.
     *
     * @param token the token
     */
    public TokenReader(String token) {
        this.token = token;
    }

    /**
     * Initialize public key.
     *
     * @param fileRepository the file repository
     * @param configBucket the config bucket
     * @throws TokenValidationException the token validation exception
     */
    public static void initializePublicKey(FileRepository fileRepository, String configBucket)
        throws TokenValidationException {

        ensureBcProviderRegistered();

        if (publicKey == null) {
            try (BufferedReader bufferedReader = fileRepository.getBufferReader(configBucket, PUBLIC_KEY_LOCATION)) {
                PEMReader pemReader = new PEMReader(bufferedReader);
                publicKey = (RSAPublicKey) pemReader.readObject();
            } catch (FileRepositoryException | IOException e) {
                throw new TokenValidationException(String.format("Error initializing public key %s/%s ", configBucket,
                    PUBLIC_KEY_LOCATION), e);
            }
        }
    }

    /**
     * Parses the.
     *
     * @return the token reader
     * @throws TokenValidationException the token validation exception
     */
    public TokenReader parse() throws TokenValidationException {
        try {
            Assert.notNull(publicKey, "Public key not initialized");
            claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token);
        } catch (SignatureException e) {
            throw new TokenValidationException("Token signature verification failed, invalid signature detected", e);
        }

        return this;
    }

    /**
     * Gets the claim.
     *
     * @param <T> the generic type
     * @param claimName the claim name
     * @param claimClass the claim class
     * @return the claim
     */
    public <T> T getClaim(String claimName, Class<T> claimClass) {
        return claims.getBody().get(claimName, claimClass);
    }

    /**
     * Ensure bouncy castle provider registered.
     */
    private static void ensureBcProviderRegistered() {
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        @SuppressWarnings("rawtypes")
        Map usuario = getClaim("usuario", Map.class);
        String email = null;

        if (usuario.containsKey("Email")) {
            email = (String) usuario.get("Email");
        }

        return email;
    }
}
