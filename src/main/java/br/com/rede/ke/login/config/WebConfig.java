/*
 * Copyright 2016 Rede S.A.
 *************************************************************
 * Nome     : WebConfig.java
 * Descrição: WebConfig.java.
 * Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 * Data     : 24/11/2016
 * Empresa  : Rede
 *********************** MODIFICAÇÕES ************************
 *Autor    : Bruno Silva <bruno.fsilva@userede.com.br>
 *Data     : 22/02/2017
 *Empresa  : Rede
 *Descrição: Configuração de filtro CORS para desenvolvimento
 *ID       : AM 188840
 *************************************************************
 */
package br.com.rede.ke.login.config;


import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class WebConfig.
 */
@Configuration
public class WebConfig {
    @Bean
    @Profile(value = "local")
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedHeader("*");
        config.addAllowedOrigin("*");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("HEADER");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        Filter filter = new CorsFilter(source);
        FilterRegistrationBean bean = new FilterRegistrationBean(filter);
        bean.setOrder(Integer.MIN_VALUE);
        return bean;
    }

}
